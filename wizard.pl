#!/usr/bin/perl
use strict;
use warnings;


my $runs = $ARGV[0] || 30;
my $cmd_template = "./gen -r ##VERTICES##  ##EDGES## 30 1";

for (my $i=0; $i < $runs ; $i++) {
	my $vertices = int(rand(1000));
	my $edges = int(rand($vertices -1));
	my $cmd = $cmd_template;
	$cmd =~ s/##VERTICES##/$vertices/;
	$cmd =~ s/##EDGES##/$edges/; 
	print "running $cmd \n";
}
