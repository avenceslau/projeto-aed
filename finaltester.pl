#!/usr/bin/perl

use strict;
use warnings;

use File::Find qw(find);
use File::Slurp;


my $workdir = './tmp2';

my $exec = 'backbone';
my $tests_dir = '../Tests2/';
my $test_res = '../Solved_Tests2/';

sub make_dirs {
        my $dir = 'tmp2';
        print "mkdir $dir\n";
        `mkdir $dir`;
}

sub compile {
        my $c_sources = '../Projeto-Aed/Projeto-Aed';
        my $cc = "gcc $c_sources/*.c -o $exec";
        print "compiling with $cc\n";
        my $cc_result = `$cc`;
        print "compiled $exec.\n";
}

make_dirs();
chdir($workdir);
my $dir = `pwd`;
print "DIR: $dir\n";
compile();

my @outs = ();
sub list_dir {
        my @dirs = @_;
        my @files;
        find({ wanted => sub { push @files, glob "\"$_/*.routes\"" } , no_chdir => 1 }, @dirs);
        return @files;
}

sub debug {
        my $s = shift;
        my $debug = 0;
        if ($debug) { print "$s\n" };
}

sub swap_extension {
        my ($fname, $orig, $dest) = @_;
        $fname =~ /(.*)$orig/;
        $fname = $1;
        $fname .= $dest;
        return $fname;
}

my $cmd;
my @files = list_dir($tests_dir);
foreach my $file (@files) {
        debug("processing file $file ....");

        my $cmd = "./$exec $file"; #/usr/bin/time -l
        debug("gonna do: $cmd....");
	print "gonna do: $cmd...";
        `$cmd`;
	print "\n";

        # mover .queries gerado para a diretoria tmp
        my $move = swap_extension($file, "routes", "bbones");
        $move = 'mv ' . $move;
        $move .= " ./";

        debug("MOVE file: $move");
        `$move`;

        # solved file
        my $solvedfile = $file;
        $solvedfile = swap_extension($solvedfile, "routes", "bbones");
        $solvedfile =~ s/Tests2/Solved_Tests2/;
        debug("solved file to compare: $solvedfile");


        my $outfile = $file;
        $outfile =~ s/.*?Tests2\///;
        $outfile = swap_extension($outfile, "routes","bbones");
        debug("outfile: $outfile");

        my $diff_cmd = "sdiff $outfile $solvedfile";# | egrep -n \"\\||<|>\"";
        $diff_cmd .= ' | egrep -n "\||>|<"';
        debug("diff_cmd: $diff_cmd");
        my $diffres = `$diff_cmd`;

        if ($diffres ne '') {
                print "DIFF_RES with $diff_cmd\n";
                print "$diffres\n";
        }
}
