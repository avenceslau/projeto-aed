#!/usr/bin/perl
use strict;
use warnings;

use File::Find qw(find);
use File::Slurp;

my $workdir = './tmp';
my $exec = 'backbone';
my $tests_dir = '../Tests/';
my $test_res = '../Solved_Tests/';


# cc compilar o codigo
sub compile {
        my $c_sources = '../Projeto-Aed/Projeto-Aed';
        my $cc = "gcc $c_sources/*.c -o $exec";
        print "compiling with $cc\n";
        my $cc_result = `$cc`;
        print "compiled $exec.\n";
}

sub make_dirs {
        my $dir = 'tmp';
        print "mkdir $dir\n";
        `mkdir $dir`;
}

# use executavel gerado e
# para cada ficheiro de teste, correr o programa
#    e comparar o resultado do executavel com o resultado esperado

make_dirs();
chdir($workdir);
my $dir = `pwd`;
print "DIR: $dir\n";
compile();

my @outs = ();
sub list_dir {
        my @dirs = @_;
        my @files;
        find({ wanted => sub { push @files, glob "\"$_/*.routes0\"" } , no_chdir => 1 }, @dirs);
        return @files;
}

sub debug {
        my $s = shift;
        my $debug = 0;
        if ($debug) { print "$s\n" };
}

sub swap_extension {
        my ($fname, $orig, $dest) = @_;
        $fname =~ /(.*)$orig/;
        $fname = $1;
        $fname .= $dest;
        return $fname;
}

# routes
my $cmd;
my @files = list_dir($tests_dir);
foreach my $file (@files) {
        debug("processing file $file ....");


        my $cmd = "./$exec $file";
        print"gonna do: $cmd....";
        `$cmd`;
        print"\n";

        # mover .queries gerado para a diretoria tmp
        my $move = swap_extension($file, "routes0", "queries");
        $move = 'mv ' . $move;
        $move .= " ./";

        debug("MOVE file: $move");
        `$move`;

        # solved file
        my $solvedfile = $file;
        $solvedfile = swap_extension($solvedfile, "routes0", "queries");
        $solvedfile =~ s/Tests/Solved_Tests/;
        debug("solved file to compare: $solvedfile");


        my $outfile = $file;
        $outfile =~ s/.*?Tests\///;
        $outfile = swap_extension($outfile, "routes0","queries");
        #$outfile =~ s/\.routes0$/\.queries/;
        debug("outfile: $outfile");

        my $diff_cmd = "sdiff $outfile $solvedfile";# | egrep -n \"\\||<|>\"";
        $diff_cmd .= ' | egrep -n "\||>|<"';
        debug("diff_cmd: $diff_cmd");
        my $diffres = `$diff_cmd`;

        if ($diffres ne '') {
                print "DIFF_RES with $diff_cmd\n";
                print "$diffres\n";
        }
        #write_file("$diff_file", $diffres);
}
