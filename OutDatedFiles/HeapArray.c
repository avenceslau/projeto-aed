//
//  HeapArray.c
//  Projeto-Aed
//
//  Created by André Venceslau on 05/12/2020.
//

#include "HeapArray.h"
#include "Matrix.h"


#define INFINITY 99999999
#define LESS(A, B) (A < B)
#define EXCH(A,B) { dijktable t = A ; A=B ; B=t;}

void Visited(dijktable * data, int vertex){
    data[KEY(vertex)].visited = true;
}

bool IsVisited (dijktable * data, int vertex){
    return data[KEY(vertex)].visited;
}

void Costprint(graphm *g, dijktable * data){
    printf("\n\n----Dijkstra Table----\n");
    for (int i = 0; i < g->n_vertex; i++) {
        printf("vertex: %d cost: %.2lf previous: %d i: %d\n", data[i].vertex, data[i].cost, data[i].previous, i);
    }
    printf("\n");
}

void Matrixprint(graphm *g){
    printf("\n\n--------Matrix--------\n");
    for (int i = 0 ; i < g->n_vertex-1 ; i++){
        for (int j = i+1 ; j < g->n_vertex ; j++){
            if ((g->matrix[Offset(i, j)] != 0) && (i != j)) {
                printf("%d %d %.2lf\n", VERTEX(i), VERTEX(j), g->matrix[Offset(i, j)]);
            }
        }
    }
}


int Partition(dijktable *a, int l, int r) {
    int i, j;
    dijktable v = a[r];
    i = l-1;
    j = r;
    for (;;) {
        while (LESS(a[++i].previous, v.previous)) ;
        while (LESS(v.previous, a[--j].previous))
            if (j == l) break;
        if (i >= j) break;
        EXCH(a[i], a[j]);
    }
    EXCH(a[i], a[r]);
    return i;
}

void QuickSortRecursive(dijktable *a, int l, int r) {
    if (r <= l) return;
    int i = Partition(a, l, r); QuickSortRecursive(a, l, i-1); QuickSortRecursive(a, i+1, r);
}

//v = vertex
dijktable * Dijkstra(graphm * g, int v){
    dijktable *data = (dijktable *) malloc(sizeof(dijktable)*g->n_vertex);
    if (data == NULL) { AbortMatrix(&g); }
    PQueue * q = CreateQueue(g->n_vertex);
    if (q == NULL) { AbortMatrix(&g); free(data);}

    AddToQueue(q, v, 0, data);
    data[KEY(v)].vertex = v;
    data[KEY(v)].previous = 0;
    data[KEY(v)].cost2 = 0;
    data[KEY(v)].visited = false;
    for (int i = 0 ; i < g->n_vertex ; i++) {
        if(i != KEY(v)){
            data[i].vertex = VERTEX(i);
            data[i].previous = 0;
            data[i].cost2 = 0;
            data[i].visited = false;
            AddToQueue(q, VERTEX(i), INFINITY, data);
        }
    }
    HeapSort(q, data);
    while (q->n_elements > 0) {
        int t = GetFirstQueue(q);
        Visited(data, t);
        RemoveMinQueue(q, data);
        QPrint(q, data);
        for (int i = 0; i < g->n_vertex ; i++) {
            if ((i != KEY(t))) {
                if(g->matrix[Offset(KEY(t), i)] != 0){
                    double alt = g->matrix[Offset(KEY(t), i)] + data[KEY(t)].cost;

                    if (alt < data[i].cost) {
                        data[i].cost2 = data[i].cost;
                        ModifyQueue(q, data[i].index, alt, data);
                        data[i].previous = t;
                    }
                }
            }
        }
    }
    QuickSortRecursive(data, 0, g->n_vertex-1);
    return data;
}

void printMST(int parent[ ],  graphm *g){
    int i;
    printf("Edge   Weight\n");
    for ( i = 1; i < g->n_vertex; i++){
        if (parent[i] != -1) {
            printf("%d - %d    %.2lf \n", min(parent[i]+1,i+1), max(parent[i]+1,i+1), g->matrix[Offset(i, parent[i])]);
        }
    }
}

int MinKey(double *cost, bool *mstSet,graphm *g){
    int v;
    double min = INFINITY;
    int min_index = 0;
    for ( v = 0; v < g->n_vertex; v++)   {
        if (mstSet[v] == false && cost[v] < min){
            min = cost[v];
            min_index = v;
        }

    }
    return min_index;
}

void PrimRec(graphm * g, int *parent, double * cost, bool * mstSet, bool * visited, int starting_index){
    cost[starting_index] = 0;
    parent[starting_index] = -1;

    for (int i = 0; i < g->n_vertex ; i++)     {
        int u = MinKey(cost, mstSet, g);
        mstSet[u] = true;
        for (int v = 0 ; v < g->n_vertex ; v++){
            if (g->matrix[Offset(u, v)] !=0 && mstSet[v] == false && g->matrix[Offset(u, v)] <  cost[v]){
                parent[v]  = u;
                cost[v] = g->matrix[Offset(u, v)];
            }
        }
    }
}

void PrimMatrix(graphm * g){
    int * parent = (int *) malloc(sizeof(int)*g->n_vertex);
    if (parent == NULL) { AbortMatrix(&g); }

    double * cost = (double *) malloc(sizeof(double)*g->n_vertex);
    if (cost == NULL) { AbortMatrix(&g); free(parent); }

    bool * mstSet = (bool *) malloc(sizeof(bool)*g->n_vertex);
    if (mstSet == NULL) { AbortMatrix(&g); free(parent); free(cost); }

    bool * visited = (bool *) malloc(sizeof(bool)*g->n_vertex);
    if (visited == NULL) { AbortMatrix(&g); free(parent); free(cost); free(mstSet); }

    for (int i = 0; i < g->n_vertex; i++){
        cost[i] = INFINITY;
        mstSet[i] = false;
        parent[i] = 0;
        visited[i] = false;
    }


    for (int i = 0; i < g->n_vertex ; i++) {
        if (mstSet[i] == false) {
            PrimRec(g, parent, cost, mstSet, mstSet, i);
        }
    }
    printMST(parent, g);
}
