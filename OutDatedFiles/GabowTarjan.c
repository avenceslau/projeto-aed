//
//  GabowTarjan.c
//  Projeto-Aed
//
//  Created by André Venceslau on 07/12/2020.
//

#include "GabowTarjan.h"
#define w sizeof(int)
#define SetBit(A,k)     ( A[(k)/32] |= (1 << ((k)%32)) )
#define ClearBit(A,k)   ( A[(k)/32] &= ~(1 << ((k)%32)) )
#define TestBit(A,k)    ( A[(k)/32] & (1 << ((k)%32)) )

typedef struct {
    int number;
    int micro;
    int id;
} microset;

//if x is not in one set creates a new set labeled x
void makeset2(int x, int *set){
    
}

int find2(int x, int*set){
    return x;
}

//the name of the new set is = to the set that contained x
//destroys sets that contained x and y
void unite(int x, int y, int *set){
    
}

//returns the microset that contains v
int micro(int v){
    return 0;
}

//receives &mark[v]
int markoffset(int micro_v, int number_v){
    return micro_v + number_v;
}

void link3(microset v, int *mark){
    
}

void link2(int v, int * parents, int*set, int root){
    if (v == root) {
        return;
    }
    unite(parents[v], v, set);
}
