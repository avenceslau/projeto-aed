//
//  HeapArray.h
//  Projeto-Aed
//
//  Created by André Venceslau on 05/12/2020.
//

#ifndef HeapArray_h
#define HeapArray_h

#include <stdio.h>
#include "Matrix.h"
#include "PQueue.h"

typedef struct {
    double cost;
    double cost2;
    int previous;
    int previous2;
} primoutput;

dijktable * Dijkstra(graphm * g, int v);
void PrimMatrix(graphm *g);

#endif /* HeapArray_h */
