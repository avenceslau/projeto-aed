//
//  PQueue.h
//  Projeto-Aed
//
//  Created by André Venceslau on 03/12/2020.
//

#ifndef PQueue_h
#define PQueue_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct {
    double cost;
    double cost2;
    int vertex;
    int previous;
    int index;
    bool visited;
} dijktable;

typedef struct {
    int * data;  //
    int n_elements;
    int size;
} PQueue;

PQueue * CreateQueue(int n_vertex);
void AddToQueue(PQueue * q, int vertex, double newcost,dijktable * cost);
void ModifyQueue(PQueue * q, int i, double newcost, dijktable * cost);
void RemoveMinQueue(PQueue * h, dijktable * cost);
void EraseQueue(PQueue * q);
int GetFirstQueue(PQueue * q);
void HeapSort(PQueue * h, dijktable * cost);

void QPrint(PQueue * q, dijktable *data);

#endif /* PQueue_h */
