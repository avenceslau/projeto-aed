//
//  PQueue.c
//  Projeto-Aed
//
//  Created by André Venceslau on 03/12/2020.
//

#include "PQueue.h"
#include "Matrix.h"
#define compare(A,B) (A<B)

double GetCost(dijktable * cost, int vertex){
    return cost[KEY(vertex)].cost;
}

int DataIndex(PQueue * q, int q_index){
    return KEY(q->data[q_index]);
}



void EraseQueue(PQueue * q){
    free(q->data);
    free(q);
}

void QPrint(PQueue * q, dijktable *data){
    printf("\n--------PQueue--------\n");
    for (int i = 0; i < q->n_elements ; i++) {
        printf("Vertex: %d cost: %.2lf index: %d index should be: %d\n", q->data[i], data[KEY(q->data[i])].cost, data[KEY(q->data[i])].index, i);
    }
    printf("\n");
}

PQueue * CreateQueue(int n_vertex){
    PQueue * q = (PQueue *) malloc(sizeof(PQueue));
    if (q == NULL) { return NULL; }
    q->n_elements = 0;
    q->size = n_vertex;
    q->data = (int *) malloc(sizeof(int)*n_vertex);
    if (q->data == NULL) {
        free(q);
        return NULL;
    }
    return q;
}

//i = index
void FixUp(PQueue *q, int i, dijktable * data) {
    int t, tmp_index;
    while ((i > 0) && compare(GetCost(data, q->data[(i - 1) / 2]), GetCost(data, q->data[i]))) {
        tmp_index = data[DataIndex(q, i)].index;
        t = q->data[i];
        
        data[DataIndex(q, i)].index = data[DataIndex(q, (i - 1) / 2)].index;
        q->data[i] = q->data[(i - 1) / 2];
        
        data[DataIndex(q, (i - 1) / 2)].index = tmp_index;
        q->data[(i - 1) / 2] = t;
        i = (i - 1) / 2;
    }
    return;
}

void FixDown(PQueue * q, int k, dijktable * data)
{
    int j;
    int t;
    int tmp_index;
    
    while ((2 * k + 1) < q->n_elements) {
        j = 2 * k + 1;
        if (((j + 1) < q->n_elements) && compare(GetCost(data, q->data[j]), GetCost(data, q->data[j + 1]))) {
            /* second offspring is greater */
            j++;
        }
        if (!compare(GetCost(data,q->data[k]), GetCost(data,q->data[j]))) {
            /* Elements are in correct order. */
            break;
        }
        /* the 2 elements are not correctly sorted, it is
         necessary to exchange them */
        tmp_index = data[DataIndex(q, k)].index;
        t = q->data[k];
        data[DataIndex(q, k)].index = data[DataIndex(q, j)].index;
        q->data[k] = q->data[j];
        
        data[DataIndex(q, j)].index = tmp_index;
        q->data[j] = t;
        k = j;
    }
    return;
}

//i = index
void SFixUp(PQueue *q, int i, dijktable * data) {
    int t, tmp_index;
    while ((i > 0) && !compare(GetCost(data, q->data[(i - 1) / 2]), GetCost(data, q->data[i]))) {
        tmp_index = data[DataIndex(q, i)].index;
        t = q->data[i];
        
        data[DataIndex(q, i)].index = data[DataIndex(q, (i - 1) / 2)].index;
        q->data[i] = q->data[(i - 1) / 2];
        
        data[DataIndex(q, (i - 1) / 2)].index = tmp_index;
        q->data[(i - 1) / 2] = t;
        i = (i - 1) / 2;
    }
    return;
}

void SFixDown(PQueue * q, int k, dijktable * data)
{
    int j;
    int t;
    int tmp_index;
    
    while ((2 * k + 1) < q->n_elements) {
        j = 2 * k + 1;
        if (((j + 1) < q->n_elements) && !compare(GetCost(data, q->data[j]), GetCost(data, q->data[j + 1]))) {
            /* second offspring is greater */
            j++;
        }
        if (compare(GetCost(data,q->data[k]), GetCost(data,q->data[j]))) {
            /* Elements are in correct order. */
            break;
        }
        /* the 2 elements are not correctly sorted, it is
         necessary to exchange them */
        tmp_index = data[DataIndex(q, k)].index;
        t = q->data[k];
        data[DataIndex(q, k)].index = data[DataIndex(q, j)].index;
        q->data[k] = q->data[j];
        
        data[DataIndex(q, j)].index = tmp_index;
        q->data[j] = t;
        k = j;
    }
    return;
}

void HeapSort(PQueue * q, dijktable * data) {
    int aux;
    int tmp_index;
    int num_elements = q->n_elements;
    for (int i = q->n_elements-1 ; i > 0 ; i--) {
        aux = q->data[0];
        tmp_index = data[DataIndex(q, 0)].index;
        
        data[DataIndex(q, 0)].index = data[DataIndex(q, i)].index;
        q->data[0] = q->data[i];
        
        data[DataIndex(q, i)].index = tmp_index;
        q->data[i] = aux;
        
        q->n_elements--;
        FixDown(q, 0, data);
    }
    q->n_elements = num_elements;
  return;
}

void AddToQueue(PQueue * q, int vertex, double newcost, dijktable * data){
    q->data[q->n_elements] = vertex;
    data[DataIndex(q, q->n_elements)].index = q->n_elements;
    data[KEY(vertex)].cost = newcost;
    q->n_elements++;
    FixUp(q, q->n_elements-1, data);
}

void ModifyQueue(PQueue * q, int q_index, double newcost, dijktable * data) {
    if (compare(newcost, GetCost(data,q->data[q_index]))) {
        data[DataIndex(q, q_index)].cost = newcost; //FIXME i <-KEY(q->data[i])
        SFixUp(q, q_index, data);
    } else {
        data[DataIndex(q, q_index)].cost = newcost; //FIXME i <-KEY(q->data[i])
        SFixDown(q, q_index, data);
    }
    return;
}

void RemoveMinQueue(PQueue * q, dijktable * data){
    if (q->n_elements > 0) {
        data[DataIndex(q, 0)].index = q->data[q->n_elements-1];
        q->data[0] = q->data[q->n_elements - 1];
        
        data[DataIndex(q, q->n_elements -1)].index = 0;
        q->data[q->n_elements - 1] = 0;
        q->n_elements--;
        SFixDown(q, 0, data); //FIXME
    }
}

int GetFirstQueue(PQueue * q){
    int t = q->data[0];
    return t;
}
