//
//  AVL_Tree.h
//  Projeto-Aed
//
//  Created by André Venceslau on 17/11/2020.
//

#ifndef AVL_Tree_h
#define AVL_Tree_h

#include <stdio.h>
#include <stdbool.h>

#define DUPLICATED_NODE -2
#define NO_MEMORY -1
#define KEY(x) (x-1)


typedef struct _node node;
typedef struct _trunk trunk;

struct _node{
    int name; //used to access trunk
    int height; //takes values between -2 and 2, is utilized to balance tree through rotations that shift the weight of a branch to another
    node * left; //left is lower
    node * right; //right is higher
    double cost; //cost to travel along a a edge
    
};

struct _trunk {
    int n_links;
    node * root;
    bool onoff;
};

trunk * CreateTrunk(int n_vertex);
int InsertBranch(node **tree,double cost, int name);
node * FindNode(node *tree, int name);
void CleanMem(trunk * table, int n_vertex);

void SeeEveryBranch(node * tree, int array[], int *i);
void SeeEveryBranchHigher(node * tree, int array[], int *i, int threshold);

node * GetRoot(trunk * table, int key);
int GetLinks(trunk * table, int key);
int GetName(node * branch);

int AVLInsert(node **tree, double cost, int name, node **root);


#endif /* AVL_Tree_h */
