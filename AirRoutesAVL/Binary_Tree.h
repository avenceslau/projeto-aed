//
//  Binary_Tree.h
//  Projeto-Aed
//
//  Created by André Venceslau on 25/10/2020.
//

#ifndef Binary_Tree_h
#define Binary_Tree_h

#include <stdio.h>
#include <stdbool.h>
#define DUPLICATED_NODE -2
#define NO_MEMORY -1

typedef struct _node node;
typedef struct _trunk trunk;

struct _node{
    int name; //used to access trunk
    int height; //saves height of the node
    node * left; //left is lower
    node * right; //right is higher
    double cost; //cost to travel along a a edge
};

struct _trunk {
    int n_links;
    node * root;
};

typedef struct{
    trunk *table;
    FILE *fp;
    int max_vertex;
} _data;

trunk * CreateTrunk(int n_vertex);
node * FindNode(node *tree, int name);
void CleanMem(trunk * table, int n_vertex);
void CutTree(node * tree);
void InitVertex(trunk *table);

void SeeEveryBranch(node * tree, int array[], int *i);

node * GetRoot(trunk * table, int key);
int GetLinks(trunk * table, int key);
int GetName(node * branch);

node * AVLInsert(node * root, int name, double cost, _data data);

#endif /* Binary_Tree_h */
