//
//  Airway_Data.c
//  Projeto-Aed
//
//  Created by André Venceslau on 24/10/2020.
//

#include "Airway_Data.h"
#include "Auxiliary_Functions.h"
#include "Binary_Tree.h"

#include <stdbool.h>
#include <string.h>

struct Input{
    int n_total_airports;
    int n_total_links;
    int var0;
    int var1;
    char mode[3];
};

//Finds one triangle
bool FindTriangle(trunk * table, _input input){
    int i = 0, j;
    int n_links = GetLinks(table, input.var0);
    int * array = (int *) malloc(sizeof(int)*n_links);
    Abort((void *) array, table, input.n_total_airports);
    
    SeeEveryBranch(GetRoot(table, input.var0), array, &i);
    for (i = 0; i < n_links-1 ; i++) {
        for (j = i ; j < n_links; j++) {
            if (FindNode(GetRoot(table, array[i]), array[j]) != NULL) {
                free(array);
                return true;
            }
        }
    }
    free(array);
    return false;
}

//Counts number of triangles
int CountTriangle(trunk * table, _input input){
    int i = 0, j;
    int triangles = 0;
    int n_links = GetLinks(table, input.var0);
    int * array = (int *) malloc(sizeof(int)*n_links);
    Abort((void *) array, table, input.n_total_airports);
    
    SeeEveryBranch(GetRoot(table, input.var0), array, &i);
    for (i = 0; i < n_links-1 ; i++) {
        for (j = i+1 ; j < n_links; j++) {
            if (FindNode(GetRoot(table, array[i]), array[j])) {
                triangles++;
            }
        }
    }
    free(array);
    return triangles;
}

//Checks existence of a vertex
bool CheckVertex(int n_airports, int vertex_to_check){
    if (vertex_to_check > n_airports || vertex_to_check < 1) {
        return false;
    } else {
        return true;
    }
}

//FIXME not in this file
void Print(_input input, FILE * fpOut){
    fprintf(fpOut, "%d %d %s ", input.n_total_airports, input.n_total_links, input.mode);
}

void ModeA0(trunk * table, _input input, FILE *fpOut){
    Print(input, fpOut);;
    if (CheckVertex(input.n_total_airports, input.var0)) {
        fprintf(fpOut, "%d %d\n", input.var0, table[input.var0-1].n_links);
    } else {
        fprintf(fpOut,"%d %d\n", input.var0, -1);
    }
}

//FIXME different file
void ModeB0(trunk * table, _input input, FILE *fpOut){
    int from = input.var0, to = input.var1;
    node * search = NULL;
    Print(input, fpOut);;
    if (CheckVertex(input.n_total_airports, from) && CheckVertex(input.n_total_airports, to)) {
        search = FindNode(GetRoot(table, from), to);
        if (search == NULL) {
              fprintf(fpOut,"%d %d %d\n", from, to, -1);
        } else {
              fprintf(fpOut, "%d %d %.2lf\n", from, to, search->cost);
        }
    } else {
          fprintf(fpOut,"%d %d %d\n", from, to, -1);
    }
}

void ModeC0(trunk * table, _input input, FILE *fpOut){
    Print(input, fpOut);;
    if (CheckVertex(input.n_total_airports, input.var0)) {
        if (FindTriangle(table, input) == true) {
              fprintf(fpOut, "%d %d\n", input.var0, 1);
        } else {
              fprintf(fpOut, "%d %d\n", input.var0, 0);
        }
    } else {
          Print(input, fpOut);
          fprintf(fpOut, "%d %d\n", input.var0, -1);
    }
}

void ModeD0(trunk * table, _input input, FILE *fpOut){
    int k = 0;
    //Print(input, fpOut);;
        if (CheckVertex(input.n_total_airports, input.var0)) {
            (k = CountTriangle(table, input));
            if (k > 0) {
                Print(input, fpOut);
                fprintf(fpOut, "%d %d\n", input.var0, k);
            } else {
                Print(input, fpOut);
                fprintf(fpOut, "%d %d\n", input.var0, 0);
            }
        } else {
            Print(input, fpOut);
            fprintf(fpOut, "%d %d\n", input.var0, -1);
        }
}

trunk * Optimize(int *max_vertex, int n_vertex, trunk * table){
    if ((*max_vertex) < n_vertex) {
        CleanMem(table, (*max_vertex));
        (*max_vertex) = n_vertex;
        table = CreateTrunk(n_vertex);
        
    } else {
        for (int i = 0; i < n_vertex; i++) {
            CutTree(table[i].root);
            InitVertex(&table[i]);
        }
    }
    return table;
}

trunk * GetData(_input input, FILE *fp, trunk * table, int max_vertex){
    int i,from = 0, to = 0;
    _data data;
    double cost = 0;
    data.table = table;
    data.max_vertex = max_vertex;
    data.fp = fp;
    
    for (i = 1; i <= input.n_total_links; i++) {
        fscanf(fp, "%d %d %lf", &from, &to, &cost);
        data.table[KEY(from)].root = AVLInsert(data.table[KEY(from)].root, to, cost, data);
        data.table[KEY(to)].root = AVLInsert(data.table[KEY(to)].root, from, cost, data);
        data.table[KEY(from)].n_links++;
        data.table[KEY(to)].n_links++;
    }
    return data.table;
}

//FIXME tem que receber ficheiro aberto different file e select mode
void SelectMode(FILE *fp, FILE *outfp){
    trunk * table = NULL;
    _input input;
    int max_vertex = 0;
    while(fscanf(fp, "%d %d %s", &input.n_total_airports, &input.n_total_links, input.mode) != EOF){
        table = Optimize(&max_vertex, input.n_total_airports, table);
        if (strcmp(input.mode, "B0") == 0) {
            fscanf(fp, "%d %d", &input.var0, &input.var1);
        }  else {
            fscanf(fp, "%d", &input.var0);
        }
        GetData(input, fp, table, max_vertex);
        if (strcmp(input.mode, "A0") == 0){ ModeA0(table, input, outfp); }
        else if (strcmp(input.mode, "B0") == 0){ ModeB0(table, input, outfp); }
        else if (strcmp(input.mode, "C0") == 0){ ModeC0(table, input, outfp); }
        else if (strcmp(input.mode, "D0") == 0){ ModeD0(table, input, outfp); }
        CleanMem(table, input.n_total_airports);
        fprintf(outfp,"\n");
    }
}

