//
//  Airway_Data.h
//  Projeto-Aed
//
//  Created by André Venceslau on 24/10/2020.
//

#ifndef Airway_Data_h
#define Airway_Data_h

#include <stdio.h>

typedef struct Input _input;
void SelectMode(FILE *fp, FILE *outfp);

#endif /* Airway_Data_h */
