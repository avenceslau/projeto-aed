//
//  Auxiliary_Functions.c
//  Projeto-Aed
//
//  Created by André Venceslau on 24/10/2020.
//

#include "Auxiliary_Functions.h"

FILE * OpenFl(char * filename, char * mode){
    FILE * fp = NULL;
    fp = fopen(filename, mode);
    
    if (fp == NULL) {
        exit(0);
    }
    return fp;
}

void Abort(void * allocated_mem, trunk * table, int n_vertex){
    if (allocated_mem == NULL) {
        CleanMem(table, n_vertex);
        exit(0);
    }
}
