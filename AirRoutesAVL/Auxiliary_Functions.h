//
//  Auxiliary_Functions.h
//  Projeto-Aed
//
//  Created by André Venceslau on 24/10/2020.
//

#ifndef Auxiliary_Functions_h
#define Auxiliary_Functions_h

#include <stdio.h>
#include <stdlib.h>
#include "Binary_Tree.h"

#define KEY(x) (x-1)

FILE * OpenFl(char * filename, char * mode);
void Abort(void * allocated_mem, trunk * table, int n_vertex);

#endif /* Auxiliary_Functions_h */
