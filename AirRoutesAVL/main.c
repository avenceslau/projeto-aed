//
//  main.c
//  Projeto-Aed
//
//  Created by André Venceslau on 24/10/2020.
//

#include <stdio.h>
#include <string.h>
#include "Auxiliary_Functions.h"
#include "Airway_Data.h"



int main(int argc, char * argv[]) {
    FILE *fp, *outfp;
    fp = OpenFl(argv[1], "r");
    
    char *outname = strrchr(argv[1],'.');
    *outname = '\0';
    outfp = OpenFl(strcat(argv[1],".queries"), "w");


    SelectMode(fp, outfp);

    fclose(fp);
    fclose(outfp);

    
    return 0;
}
