//
//  AirRoutes0.h
//  Aed-Project
//
//  Created by André Venceslau and Alexandre Lopes on 03/12/2020.
//

#ifndef AirRoutes0_h
#define AirRoutes0_h

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

typedef struct Input _input;
struct Input{
    int n_total_airports;
    int n_total_links;
    int var0;
    int var1;
    char mode[3];
};

void select_mode_0(FILE *fp, FILE *outfp);
void print_header(_input input, FILE * fpOut);
bool check_vertex(int n_airports, int vertex_to_check);

#endif /* AirRoutes0_h */
