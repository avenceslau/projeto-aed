//
//  Matrix.h
//  Aed-Project
//
//  Created by André Venceslau and Alexandre Lopes on 19/11/2020.
//

#ifndef Matrix_h
#define Matrix_h

#include <stdio.h>
#include <stdbool.h>

#define KEY(x) (x-1)
#define VERTEX(x) (x+1)
#define max(i,j) (((i) >= (j)) ? (i) : (j))
#define min(i,j) (((i) <= (j)) ? (i) : (j))

typedef struct {
    double *matrix;
    int n_vertex;
    int n_links;
} graphm;

int offset(int i, int j);
void matrix_get_data(int n_links , FILE *fp, graphm ** g, int max_vertex);
double * create_matrix(int n_vertex);
void init_matrix(graphm **G);
void erase_matrix(graphm **g);
void abort_matrix(graphm **g);

#endif /* Matrix_h */
