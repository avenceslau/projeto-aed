//
//  main.c
//  Aed-Project
//
//  Created by André Venceslau on 10/12/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "AirRoutes0.h"
#include "AirRoutes.h"

/**
 * @brief Wrapper for the fopen function. Tries to open a file on a fail exits on a success returns a pointer to the file.
 *
 * @param filename string with the name of the file
 * @param mode type 
 * @return FILE* 
 */
FILE * open_fl(char * filename, const char * mode){
    FILE * fp = NULL;
    fp = fopen(filename, mode);
    
    if (fp == NULL) {
        exit(0);
    }
    return fp;
}

/**
 * @brief Checks if the file has the right extension
 *        Returns true if the file has the right extension or false if the extension is wrong
 * 
 * @param file_ext Extension of the file
 * @param accepted_ext Expected extension 
 * @return true 
 * @return false 
 */
bool check_problem(char * file_ext, const char * accepted_ext) {
    if (!strcmp(file_ext, accepted_ext)) {
        return true;
    } else {
        return false;
    }
}


int main(int argc, const char * argv[]) {
    FILE *fp, *outfp = NULL;
    char * filename = (char *) malloc(strlen(argv[1])+1);
    strcpy(filename, argv[1]);
    fp = open_fl(filename, "r");
    
    /* Retrieves the file extension */
    char *outname = strrchr(filename,'.');
    if(check_problem(outname, ".routes0")){
        *outname = '\0';
        strcat(filename,".queries");
        outfp = open_fl(filename, "w");
        free(filename);
        select_mode_0(fp, outfp);
    } else if (check_problem(outname, ".routes")){
        *outname = '\0';
        strcat(filename,".bbones");
        outfp = open_fl(filename, "w");
        free(filename);
        select_mode(fp, outfp);
    }
    fclose(fp);
    fclose(outfp);
    return 0;
}
