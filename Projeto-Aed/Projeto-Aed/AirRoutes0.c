//
//  AirRoutes0.c
//  Aed-Project
//
//  Created by André Venceslau and Alexandre Lopes on 03/12/2020.
//

#include "AirRoutes0.h"
#include "Matrix.h"

//Print dos valores do cabeçalho
void print_header(_input input, FILE * fpOut){
    fprintf(fpOut, "%d %d %s", input.n_total_airports, input.n_total_links, input.mode);
}


//Verifica a existência de um nodo
bool check_vertex(int n_airports, int vertex_to_check){
    if (vertex_to_check > n_airports || vertex_to_check < 1) {
        return false;
    } else {
        return true;
    }
}

//Encontra um "triângulo" na matriz
bool find_triangle(double * matrix, graphm * g, int vertex){
    int n_vertex = g->n_vertex;
    
    int l = matrix[offset(KEY(vertex), KEY(vertex))];
    int * array = (int *) malloc(sizeof(int)*l);
    
    for (int i = 0, t = 0; i < n_vertex; i++) {
        if ((matrix[offset(KEY(vertex), i)] != 0) && (i != KEY(vertex))) {
            array[t] = i+1;
            t++;
            
        }
    }
    
    for (int i = 0 ; i < l -1; i++) {
        for (int j = i+1; j < l; j++) {
            if (matrix[offset(KEY(array[i]) , KEY(array[j]))] != 0){
                if (array != NULL) {
                    free(array);
                }
                return true;
            }
        }
    }
    if (array != NULL) {
        free(array);
    }
    return false;
}


//Conta número de triângulos
int count_triangle(double * matrix, graphm * g, _input input){
    int triangles = 0;
    int n_links = matrix[offset(KEY(input.var0), KEY(input.var0))];
    int * array = (int *) malloc(sizeof(int)*n_links);
    int j;
    if (array == NULL) {
        free(array);
        abort_matrix(&g);
    }
    
    for (int i = 0, t = 0; i < g->n_vertex; i++) {
        if ((matrix[offset(KEY(input.var0), i) ] != 0) && (i != KEY(input.var0))) {
            array[t] = i+1;
            t++;
        }
    }
    
    for (int i = 0; i < n_links-1 ; i++) {
        for (j = i+1 ; j < n_links; j++) {
            if (matrix[offset(KEY(array[i]), KEY(array[j]))] != 0) {
                triangles++;
            }
        }
    }
    if (array != NULL) {
        free(array);
    }
    return triangles;
}


//Função para modo A0
void A0(graphm *g, _input input, FILE *fpOut){
    double * matrix = g->matrix;
    print_header(input, fpOut);
    if (check_vertex(input.n_total_airports, input.var0)) {
        fprintf(fpOut, " %d %d\n", input.var0, (int) matrix[offset(KEY(input.var0), KEY(input.var0))]);
    } else {
        fprintf(fpOut," %d %d\n", input.var0, -1);
    }
}

//Função para modo B0
void B0(graphm * g, _input input, FILE *fpOut){
    int from = input.var0, to = input.var1;
    double * matrix = g->matrix;
    print_header(input, fpOut);
    if (check_vertex(input.n_total_airports, from) && check_vertex(input.n_total_airports, to) && (input.var0 != input.var1)) {
        if (matrix[offset(KEY(from), KEY(to))] == 0) {
              fprintf(fpOut," %d %d %d\n", from, to, -1);
        } else {
              fprintf(fpOut, " %d %d %.2lf\n", from, to, matrix[offset(KEY(from), KEY(to))]);
        }
    } else {
          fprintf(fpOut," %d %d %d\n", from, to, -1);
    }
}

//Função para modo C0
void C0(graphm *g, _input input, FILE *fpOut){
    print_header(input, fpOut);
    if (check_vertex(input.n_total_airports, input.var0)) {
        if (find_triangle(g->matrix, g, input.var0) == true) {
              fprintf(fpOut, " %d %d\n", input.var0, 1);
        } else {
              fprintf(fpOut, " %d %d\n", input.var0, 0);
        }
    } else {
          fprintf(fpOut, " %d %d\n", input.var0, -1);
    }
}

//Função para modo D0
void D0(graphm * g, _input input, FILE *fpOut){
    int k = 0;
    print_header(input, fpOut);;
        if (check_vertex(input.n_total_airports, input.var0)) {
            k = count_triangle(g->matrix, g, input);
            if (k > 0) {
                fprintf(fpOut, " %d %d\n", input.var0, k);
            } else {
                fprintf(fpOut, " %d %d\n", input.var0, 0);
            }
        } else {
            fprintf(fpOut, " %d %d\n", input.var0, -1);
        }
}

//Seleciona o modo (A0,B0,C0,D0). Dependendo do modo lê do ficheiro 1 ou 2 valores de input ( var0 e var1)
void select_mode_0(FILE *fp, FILE *outfp){
    _input input;
    graphm *g = (graphm *) malloc(sizeof(graphm));
    if (g == NULL) {
        return;
    }
    
    int max_vertex = 0;
    
    while(fscanf(fp, "%d %d %s", &input.n_total_airports, &input.n_total_links, input.mode) != EOF){
        g->n_vertex = input.n_total_airports;
        g->matrix = create_matrix(g->n_vertex);
        g->n_links = input.n_total_links;
        init_matrix(&g);
        if (strcmp(input.mode, "B0") == 0) {
            fscanf(fp, "%d %d", &input.var0, &input.var1);
        }  else {
            fscanf(fp, "%d", &input.var0);
        }
        
        matrix_get_data(input.n_total_links , fp, &g, max_vertex);
        if (strcmp(input.mode, "A0") == 0){
            A0(g, input, outfp);
        } else if (strcmp(input.mode, "B0") == 0){
            B0(g, input, outfp);
        } else if (strcmp(input.mode, "C0") == 0){
            C0(g, input, outfp);
        } else if (strcmp(input.mode, "D0") == 0){
            D0(g, input, outfp);
        }
        fprintf(outfp,"\n");
        erase_matrix(&g);
    }
    if (g != NULL) {
        free(g);
    }
}
