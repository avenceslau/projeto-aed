//
//  EdgeList.c
//  Aed-Project
//
//  Created by André Venceslau on 10/12/2020.
//

#include "EdgeList.h"
#include <stdlib.h>

//Reset do grafo
void erase_graph(graphl **g){
    (*g)->n_links = 0;
    (*g)->n_vertex = 0;
    free((*g)->elist->data);
}

//Inicialização do grafo
void init_graph (graphl **g, int n_vertex, int n_links){
    (*g)->elist->data = (edge *) malloc(sizeof(edge)*n_links);
    (*g)->n_links = n_links;
    (*g)->n_vertex = n_vertex;
    (*g)->elist->mst = 0;
    (*g)->elist->n_links = 0;
}

//Criação de um grafo
graphl * create_graph(){
    graphl *g = (graphl *) malloc(sizeof(graphl));
    g->elist = (edge_list *) malloc(sizeof(edge_list));
    g->n_links = 0;
    g->n_vertex = 0;
    return g;
}

//Guarda os custos das ligações no vetor de ligações
void elist_store_data(int from, int to, double cost, graphl *g, int i){
    g->elist->data[i].u = min(KEY(from), KEY(to));
    g->elist->data[i].v = max(KEY(from), KEY(to));
    g->elist->data[i].cost = cost;
}

//Procura no ficheiro de entrada as ligações e o seu respetivo custo, e guarda-os na lista
edge elist_get_data(graphl **g, FILE *fp, edge e){
    int from = 0, to = 0;
    double cost = 0;
    e.cost = 0;
    for (int i = 0 ; i < (*g)->n_links ; i++) {
        fscanf(fp, "%d %d %lf", &from, &to, &cost);
        elist_store_data(from, to, cost, (*g), i);
        if((e.u == (*g)->elist->data[(*g)->elist->n_links].u) && (e.v == (*g)->elist->data[(*g)->elist->n_links].v)){
            e.cost = (*g)->elist->data[(*g)->elist->n_links].cost;
        }
        (*g)->elist->n_links++;
    }
    return e;
}
