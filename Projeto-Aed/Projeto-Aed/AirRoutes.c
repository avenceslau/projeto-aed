//
//  AirRoutes.c
//  Aed-Project
//
//  Created by André Venceslau on 10/12/2020.
//

#include "AirRoutes.h"
#include "Sets.h"
#include "AirRoutes0.h"


#define EXCH(A,B) { edge t = A ; A=B ; B=t;}

/**
 * @brief Checks if A is greater than B
 * 
 * @param A first val 
 * @param B second
 * @return true 
 * @return false 
 */
bool GREATER(double A, double B){
    return (((A) > (B)) ? (true) : (false));
}

/**
 * @brief Compare function to be used in qsort
 * 
 * @param A pointer to struct edge
 * @param B pointer to struct edge 
 * @return int -1 if A is greater than B, or 1 if is smaller 
 */
int cmp_cost_greater(const void * A, const void * B){
    if (GREATER((*((edge *) A)).cost, (*((edge *) B)).cost) == true) {
        return -1;
    }
    return 1;
}

/**
 * @brief Checks if A is smaller than B
 * 
 * @param A first val
 * @param B secon val
 * @return true 
 * @return false 
 */
bool LESS(double A, double B){
    return (((A) < (B)) ? (true) : (false));
}

/**
 * @brief Compare function to be used in qsort
 * 
 * @param A pointer to struct edge
 * @param B pointer to struct edge 
 * @return int -1 if A is smaller than B, or 1 if is greater 
 */
int cmp_cost_lesser(const void * A, const void * B){
    if (LESS((*((edge *) A)).cost, (*((edge *) B)).cost) == true) {
        return -1;
    }
    return 1;
}

/**
 * @brief Compares the value of both vertexes in an edge with vertexes of another edge 
 * 
 * @param a edge 1
 * @param b edge 2
 * @return true 
 * @return false 
 */
bool compare_edge(edge a, edge b){
    if(a.u < b.u){
        return true;
    } else if (a.u == b.u) {
        if (a.v < b.v) {
            return true;
        }
    }
    return false;
}

/**
 * @brief Compares the value of both vertexes in an edge with vertexes of another edge
 *        Used in qsort 
 * 
 * @param A edge 1
 * @param B edge 2
 * @return int 
 */
int cmp_edge(const void * A, const void * B){
    edge a = *((edge *) A);
    edge b = *((edge *) B);
    
    if(a.u < b.u){
        return -1;
    } else if (a.u == b.u) {
        if (a.v < b.v) {
            return -1;
        }
    }
    return 1;
}

/**
 * @brief Prints the result of the E1 mode, is complyent with the requirements of the project
 * 
 * @param elist initial edge_list
 * @param mst mst edges
 * @param r_edges replacement edges
 * @param g graph
 * @param fpOut pointer to output file
 */
void print_E1(edge_list *elist, edge_list mst, edge_list r_edges, graphl *g, FILE * fpOut){
    int counter = 0;
    for (int i = elist->mst-1 ; i < g->n_links ; i++) {
        if (counter < r_edges.n_links && elist->data[i].u == mst.data[counter].u && elist->data[i].v == mst.data[counter].v) {
            fprintf(fpOut, "%d %d %.2lf %d %d %.2lf\n",
                    VERTEX(elist->data[i].u), VERTEX(elist->data[i].v), elist->data[i].cost,
                    VERTEX(r_edges.data[counter].u), VERTEX(r_edges.data[counter].v), find_cost(elist, r_edges.data[counter]));
            counter++;
        } else {
            fprintf(fpOut, "%d %d %.2lf -1\n", VERTEX(elist->data[i].u), VERTEX(elist->data[i].v), elist->data[i].cost);
        }
    }
}

/**
 * @brief Partition function used inside quicksort.
 *        Orders two arrays at the same time based 
 * 
 * @param a edge_list 1
 * @param b edge_list 2
 * @param l left boundry
 * @param r right boundry
 * @return int 
 */
int PartitionTWOEDGES(edge_list *a, edge_list *b,  int l, int r) {
    int i, j;
    edge e = a->data[r];
    i = l-1;
    j = r;
    for (;;) {
        while (compare_edge(a->data[++i], e));
        while (compare_edge(e, a->data[--j])){
            if (j == l) break;
        }
        if (i >= j) break;
        EXCH(a->data[i], a->data[j]);
        EXCH(b->data[i], b->data[j]);
    }
    EXCH(a->data[i], a->data[r]);
    EXCH(b->data[i], b->data[r]);
    return i;
}

/**
 * @brief Orders two dependent edge_list based on the 1st edgelist
 *        The lists are ordered from the smallest to the biggest
 * 
 * @param a edge_list a
 * @param b edge_list b
 * @param l lower bound of the array
 * @param r upper bound of the array
 */
void QuickSortTWOEDGES(edge_list *a, edge_list *b,  int l, int r) {
    if (r <= l) return;
    int i = PartitionTWOEDGES(a, b,l, r);
    QuickSortTWOEDGES(a, b,l, i-1);
    QuickSortTWOEDGES(a, b,i+1, r);
}

/**
 * @brief prints a edge_list to a file
 * 
 * @param e pointer to edge_list
 * @param start starting index
 * @param end final index
 * @param g pointer to graph
 * @param fpOut pointer to output file
 */
 void print_edge_list(edge_list * e, int start, int end, graphl *g, FILE *fpOut){
    for (int i = start; i < end; i++) {
        fprintf(fpOut, "%d %d %.2lf\n", VERTEX(e->data[i].u), VERTEX(e->data[i].v), e->data[i].cost);
    }
}

/**
 * @brief Checks if the input variables from the problem are valid
 * 
 * @param input structure containing the input
 * @return true 
 * @return false 
 */
bool check_input(_input input){
    if (check_vertex(input.n_total_airports, input.var0) && check_vertex(input.n_total_airports, input.var1) && (input.var0 != input.var1)) {
        return true;
    } else {
        return false;
    }
}

/**
 * @brief Finds the cost of an edge
 * 
 * @param elist edge_list
 * @param e edge
 * @return double cost of the edge 
 */
double find_cost(edge_list * elist, edge e){
    double cost = 0;
    for (int i = 0 ; i < elist->n_links ; i++) {
        if (elist->data[i].u == e.u && elist->data[i].v == e.v) {
            return elist->data[i].cost;
        }
    }
    return cost;
}
/**
 * @brief finds the replacement for the edge and returns the replacement if it exists
 * 
 * @param e edge
 * @param r_edges edge_list
 * @param MST minimum spanning tree
 * @return edge 
 */
edge find_replacement(edge e, edge_list r_edges, edge_list MST){
    for (int i = 0; i < r_edges.n_links ; i++) {
        if (MST.data[i].u == e.u && MST.data[i].v == e.v) {
            return r_edges.data[i];
        }
    }
    edge z;
    z.u = -1;
    z.v = -1;
    return z;
}

/**
 * @brief Get the mst from a graph and its total cost
 * 
 * @param g graph 
 * @param total_cost total_cost of the mst is return through the variable  
 * @return edge_list 
 */
edge_list get_mst(graphl *g, double * total_cost){
    edge_list mst;
    mst.data = (edge *) malloc(sizeof(edge)*(g->n_links - g->elist->n_links));

    mst.n_links = g->n_links - g->elist->n_links;
    (*total_cost) = 0;
    for (int i = 0; i < mst.n_links; i++) {
        mst.data[i] = g->elist->data[i+(g->elist->mst)-1];
        (*total_cost) += g->elist->data[i+(g->elist->mst)-1].cost;
    }
    return mst;
}

/**
 * @brief Finds the different strongly connected components in an edgelist and returns it
 *        Also returns the number of components
 * 
 * @param edges edge_list
 * @param g graph structure
 * @param counter number of strongly connected components
 * @return int* array with components
 */
int *find_components(edge_list *edges, graphl * g, int * counter){
    int * set = (int *) calloc(sizeof(int), g->n_vertex);
    int * sz = (int *) calloc(sizeof(int), g->n_vertex);
    int * components = (int *) calloc(sizeof(int), g->n_vertex);
    
    for (int i = 0; i < g->n_vertex; i++) {
        make_set(i, set, sz);
        components[i] = -1;
    }
    
    int start_mst = g->n_links - edges->n_links;
    
    for (int i = 0; i < start_mst ; i++) {
        set_union(edges->data[i+edges->mst-1].u, edges->data[i+edges->mst-1].v, set, sz);
    }
    
    (*counter) = 0;
    int aux;
    free(sz);
    for (int i = 0; i < g->n_vertex; i++) {
        aux = find_compressed(i, set);
        if (aux != components[aux]) {
            components[aux] = aux;
            (*counter)++;
        }
    }
    free(set);
    int *rep = (int *) calloc(sizeof(int), (*counter));
    int end = (*counter);
    (*counter) = 0;
    for (int i = 0; (*counter) < end ; i++) {
        if (components[i] != -1) {
            rep[(*counter)] = components[i];
            (*counter)++;
        }
    }
    free(components);
    return rep;
}

/**
 * @brief Solves A1 mode
 * 
 * @param g graph
 * @param input input
 * @param fpOut pointer to output file
 */
void A1(graphl * g, _input input, FILE * fpOut){
    double total_cost;
    
    kruskal_algo(g);
    edge_list MST = get_mst(g, &total_cost);
    qsort(MST.data, MST.n_links, sizeof(edge), cmp_edge);
    print_header(input, fpOut);
    fprintf(fpOut, " %d %.2lf\n", MST.n_links, total_cost);
    print_edge_list(&MST, 0, MST.n_links, g, fpOut);
    free(MST.data);
}

/**
 * @brief Solves B1 mode
 * 
 * @param g graph
 * @param input input
 * @param fpOut pointer to output file
 * @param t edge to replace
 */
void B1(graphl *g, _input input, FILE *fpOut, edge t){
    edge_list mst, r_edges;
    edge e, replacement;
    replacement.u = -1; replacement.v = -1;
    bool found = false;
    double total_cost = 0;
    
    print_header(input, fpOut);
    kruskal_algo(g);
    mst = get_mst(g, &total_cost);
    qsort(mst.data, mst.n_links, sizeof(edge), cmp_edge);
    fprintf(fpOut, " %d %d %d %.2lf",input.var0,input.var1, mst.n_links, total_cost);
    r_edges.n_links = 0;
    if (check_input(input)) {
        int u = min(input.var0, input.var1), v = max(input.var0, input.var1);
        for (int i = 0; i < mst.n_links ; i++) {
            if (u == VERTEX(mst.data[i].u) && v == VERTEX(mst.data[i].v) && !found) {
                found = true;
                e = mst.data[i];
                break;
            }
        }
        if (found){
            r_edges.data = (edge *) malloc(sizeof(edge)*(mst.n_links));
            find_replacement_edges(g, g->elist, &mst, e.v, &r_edges);
            replacement = find_replacement(e, r_edges, mst);
            free(r_edges.data);
        }
        if ((replacement.u == -1 && !t.cost) || (replacement.u == -1 && found)) {
            fprintf(fpOut, " -1\n");
        } else if (t.cost && replacement.u == -1){
            fprintf(fpOut, " 0\n");
        } else {
            fprintf(fpOut, " 1\n");
        }
    } else {
        fprintf(fpOut, " -1\n");
    }
    edge * mst_edges = g->elist->data + g->elist->mst - 1;
    qsort(mst_edges, mst.n_links, sizeof(edge), cmp_edge);
    print_edge_list(g->elist, g->elist->mst-1, g->n_links, g, fpOut);
    if (replacement.u != -1) {
        fprintf(fpOut, "%d %d %.2lf\n", VERTEX(replacement.u), VERTEX(replacement.v), find_cost(g->elist, replacement));
    }
    free(mst.data);
}

/**
 * @brief Solves C1 mode
 * 
 * @param g graph
 * @param input input
 * @param fpOut pointer to output file
 * @param t edge to exclude
 */
void C1(graphl * g, _input input, FILE * fpOut, edge t){
    edge_list MST, r_edges;
    edge e, replacement;
    double total_cost;
    bool found = false;

    
    replacement.u = -1; replacement.v = -1;
    r_edges.n_links = 0;
    print_header(input, fpOut);
    kruskal_algo(g);
    MST = get_mst(g, &total_cost);
    qsort(MST.data, MST.n_links, sizeof(edge), cmp_edge);
    fprintf(fpOut, " %d %d %d %.2lf", input.var0, input.var1, MST.n_links, total_cost);
    if (check_input(input)) {
        int u = min(input.var0, input.var1), v = max(input.var0, input.var1);
        for (int i = 0; i < MST.n_links ; i++) {
            if (u == VERTEX(MST.data[i].u) && v == VERTEX(MST.data[i].v) && !found) {
                found = true;
                e = MST.data[i];
                break;
            }
        }
        if (found){
            r_edges.data = (edge *) malloc(sizeof(edge)*(MST.n_links));
            find_replacement_edges(g, g->elist, &MST, e.v, &r_edges);
            replacement = find_replacement(e, r_edges, MST);
            free(r_edges.data);
        }
    }
    if (!t.cost || !found) {
        fprintf(fpOut, " -1\n");
    }
    else {
        if (replacement.u == -1) {
            fprintf(fpOut, " %d %.2lf\n", MST.n_links-1, (total_cost-(t.cost)));
        } else {
            replacement.cost = find_cost(g->elist, replacement);
            fprintf(fpOut, " %d %.2lf\n", MST.n_links, (total_cost-(t.cost)+replacement.cost));
        }
        
    }
    edge * mst_edges = g->elist->data + g->elist->mst - 1;
    qsort(mst_edges, MST.n_links, sizeof(edge), cmp_edge);
    print_edge_list(g->elist, g->elist->mst-1, g->n_links, g, fpOut);
    if (replacement.u != -1 || found) {
        for (int i = g->elist->mst-1 ; i < g->n_links ; i++) {
            if (e.u == g->elist->data[i].u && e.v == g->elist->data[i].v) {
                g->elist->data[i] = replacement;
            }
        }
        qsort(mst_edges, MST.n_links, sizeof(edge), cmp_edge);
        if (replacement.u == -1) {
            print_edge_list(g->elist, g->elist->mst, g->n_links, g, fpOut);
        } else {
            print_edge_list(g->elist, g->elist->mst-1, g->n_links, g, fpOut);
        }
        
        
        
    }
    free(MST.data);
}

/**
 * @brief Solves D1 mode
 * 
 * @param g graph 
 * @param input input
 * @param fpOut pointer to output file
 */
void D1(graphl * g, _input input, FILE * fpOut){
    double total_cost;
    int result = 0;

    kruskal_algo(g);
    edge_list mst = get_mst(g, &total_cost);
    qsort(mst.data, mst.n_links, sizeof(edge), cmp_edge);
    int r_vertex = input.var0;
    if(!check_vertex(g->n_vertex, r_vertex)){
        result = -1;
    }

    edge_list newlist;
    newlist.data = (edge *) malloc(sizeof(edge)*g->n_links);
    newlist.n_links = 0;
    connectscc(KEY(r_vertex), g->elist, mst, g, &result, &newlist);
    print_header(input,fpOut);
    qsort(newlist.data, newlist.n_links, sizeof(edge), cmp_edge);
    fprintf(fpOut," %d %d %.2lf %d\n", r_vertex, mst.n_links, total_cost, result);
    for ( int i = 0; i < mst.n_links; i++){
        fprintf(fpOut,"%d %d %.2lf\n", VERTEX(mst.data[i].u), VERTEX(mst.data[i].v), mst.data[i].cost);
    }
    for ( int i = 0; i < newlist.n_links; i++){
        fprintf(fpOut,"%d %d %.2lf\n", VERTEX(newlist.data[i].u), VERTEX(newlist.data[i].v), newlist.data[i].cost);
    }
    free(mst.data);
    free(newlist.data);
}

/**
 * @brief Solves E1 Mode
 * 
 * @param g graph 
 * @param input input
 * @param fpOut pointer to output file
 */
void E1(graphl * g, _input input, FILE * fpOut){
    edge_list mst, r_edges;
    int counter = 0;
    double total_cost;
    kruskal_algo(g);
    edge * mst_edges = g->elist->data + g->elist->mst - 1;
    qsort(mst_edges, mst.n_links, sizeof(edge), cmp_cost_lesser);
    int *rep = find_components(g->elist, g, &counter);
    print_header(input, fpOut);
    mst = get_mst(g, &total_cost);
    fprintf(fpOut, " %d %.2lf\n", mst.n_links, total_cost);
    r_edges.data = (edge *) malloc(sizeof(edge)*(mst.n_links));
    r_edges.n_links = 0;
    /* for each edge in the mst finds its replacement */
    for (int i = 0; i < counter; i++) {
        find_replacement_edges(g, g->elist, &mst, rep[i], &r_edges); //instead of 0 takes redges.n_links
    }
    QuickSortTWOEDGES(&mst, &r_edges, 0, r_edges.n_links-1);
    mst_edges = g->elist->data + g->elist->mst - 1;
    qsort(mst_edges, mst.n_links, sizeof(edge), cmp_edge);
    print_E1(g->elist, mst, r_edges, g, fpOut);
    free(rep);
    free(mst.data);
    free(r_edges.data);
}

/**
 * @brief Finds what the mode is and solves it
 * 
 * @param fp pointer to input file
 * @param outfp pointer to output file
 */
void select_mode(FILE *fp, FILE *outfp){
    _input input;
    graphl *g;
    edge t;
    t.u = 0;
    t.v = 0;
    t.cost = 0;
    g = create_graph();
    while(fscanf(fp, "%d %d %s", &input.n_total_airports, &input.n_total_links, input.mode) != EOF){
        init_graph(&g, input.n_total_airports,input.n_total_links);
        if ((strcmp(input.mode, "B1")==0 ) || (strcmp(input.mode, "C1") == 0)) {
            fscanf(fp, "%d %d", &input.var0, &input.var1);
            t.u = KEY(min(input.var0, input.var1)); t.v = KEY(max(input.var0, input.var1));
        }  else if (strcmp(input.mode, "D1")==0){
            fscanf(fp, "%d", &input.var0);
        }
        
        t = elist_get_data(&g, fp, t);
        if (strcmp(input.mode, "A1") == 0){
            A1(g, input, outfp);
        } else if (strcmp(input.mode, "B1") == 0){
            B1(g, input, outfp, t);
        } else if (strcmp(input.mode, "C1") == 0){
            C1(g, input, outfp, t);
        } else if (strcmp(input.mode, "D1") == 0){
            D1(g, input, outfp);
        } else if (strcmp(input.mode, "E1") == 0){
            E1(g, input, outfp);
        }
        fprintf(outfp,"\n");
        erase_graph(&g);
    }
    if (g != NULL) {
        free(g->elist);
        free(g);
    }
}
