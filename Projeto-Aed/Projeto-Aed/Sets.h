//
//  Sets.h
//  Aed-Project
//
//  Created by André Venceslau on 10/12/2020.
//

#ifndef Sets_h
#define Sets_h

#include <stdio.h>
int find_compressed(int x, int*set);
void set_union(int x, int y, int * set, int * sz);
void link_set(int x, int y, int * set);
void make_set(int x, int*set, int *sz);
void make_set_no_size(int x, int*set);
int find_size(int x, int *sz, int * set);

#endif /* Sets_h */
