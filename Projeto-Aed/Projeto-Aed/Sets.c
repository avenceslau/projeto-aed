//
//  Sets.c
//  Aed-Project
//
//  Created by André Venceslau on 10/12/2020.
//

#include "Sets.h"

/**
 * @brief Returns the parent of the input x 
 *        and compresses the set
 * 
 * @param x input 
 * @param set set
 * @return int parent
 */
int find_compressed(int x, int*set){
    int root = x;
    while (root != set[root]) {
        root = set[root];
    }
    while (x != root) {
        int next = set[x];
        set[x] = root;
        x = next;
    }
    return root;
}

/**
 * @brief Weighted quick union
 * 
 * @param x x 
 * @param y y
 * @param set set 
 * @param sz sizes
 */
void set_union(int x, int y, int * set, int * sz){
    x = find_compressed(x, set);
    y = find_compressed(y, set);
    
    if (x == y) {
        return;
    }
    if (sz[x] < sz[y]) {
        set[x] = y;
        sz[y] = sz[x] + sz[y];
    } else {
        set[y] = x;
        sz[x] = sz[x] + sz[y];
    }
}

/**
 * @brief Link set quick union c used in Bader's Algorithm
 * 
 * @param x x
 * @param y y
 * @param set set 
 */
void link_set(int x, int y, int * set){
    x = find_compressed(x, set);
    y = find_compressed(y, set);
    if (x == y) {
        return;
    }
    set[y] = x;
}

/**
 * @brief initializes x in the set
 * 
 * @param x x
 * @param set set
 * @param sz sizes
 */
void make_set(int x, int*set, int *sz){
    if (set[x] == 0) {
        set[x] = x;
        sz[x] = 1;
    }
}

/**
 * @brief Initializes a set with no size
 * 
 * @param x x
 * @param set set
 */
void make_set_no_size(int x, int*set){
    if (set[x] == 0) {
        set[x] = x;
    }
}

/**
 * @brief Finds the size on a weighted quick union
 * 
 * @param x x
 * @param sz sizes
 * @param set set
 * @return int 
 */
int find_size(int x, int *sz, int * set){
    return sz[find_compressed(x, set)];
}
