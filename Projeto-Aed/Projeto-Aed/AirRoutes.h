//
//  AirRoutes.h
//  Aed-Project
//
//  Created by André Venceslau on 10/12/2020.
//

#ifndef AirRoutes_h
#define AirRoutes_h

#include <stdio.h>
#include "EdgeList.h"
#include "mst_manipulation.h"

void select_mode(FILE *fp, FILE *outfp);
bool compare_edge(edge a, edge b);
double find_cost(edge_list *elist, edge e);
int cmp_cost_greater(const void * A, const void * B);
int cmp_cost_lesser(const void * A, const void * B);

#endif /* AirRoutes_h */
