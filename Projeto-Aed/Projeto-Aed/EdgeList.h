//
//  EdgeList.h
//  Aed-Project
//
//  Created by André Venceslau on 10/12/2020.
//

#ifndef EdgeList_h
#define EdgeList_h

#include <stdio.h>

#define KEY(x) (x-1)
#define VERTEX(x) (x+1)
#define max(i,j) (((i) >= (j)) ? (i) : (j))
#define min(i,j) (((i) <= (j)) ? (i) : (j))


typedef struct edge {
    double cost;
    int u, v;
} edge;

typedef struct edge_list {
    edge *data;
    int n_links;
    int mst;
} edge_list;

typedef struct{
    int n_links;
    int n_vertex;
    edge_list *elist;
} graphl;

void erase_graph(graphl **g);
void init_graph (graphl **g, int n_vertex, int n_links);
graphl * create_graph(void);
edge elist_get_data(graphl **g, FILE *fp, edge e);

#endif /* EdgeList_h */
