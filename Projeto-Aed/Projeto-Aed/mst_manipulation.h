//
//  FindPathNMatrix.h
//  Aed-Project
//
//  Created by André Venceslau on 10/12/2020.
//

#ifndef mst_manipulation_h
#define mst_manipulation_h


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "EdgeList.h"



void kruskal_algo(graphl *g);
void QuickSortElist(edge_list *a, int l, int r, bool compare(double, double));
bool LESS(double A, double B);
bool GREATER(double A, double B);
int SOffset(edge a);
void find_replacement_edges(graphl *g, edge_list *nonMST, edge_list *MST, int v, edge_list * r_edges);
void CleanEdgeList(edge_list *e);
int * findscc(edge_list elist, int r_vertex, graphl *g, int *sz);
void connectscc(int r_vertex, edge_list* elist, edge_list mst, graphl *g, int * result, edge_list *newlist);

#endif /* mst_manipulation_h */
