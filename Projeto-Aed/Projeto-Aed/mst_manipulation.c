//
//  mst_manipulation.c
//  Aed-Project
//
//  Created by André Venceslau on 10/12/2020.
//

#include "mst_manipulation.h"
#include "Sets.h"
#include "AirRoutes.h"


#define EXCH(A,B) { edge t = A ; A=B ; B=t;}

/**
 * @brief Receives an edge list and the index of the edge to be omitted from the edge_list
 *        To remove the edge, we simply move subtract one fromthe number of elements from 
 *        the list and switch the edge that is going to be removed with the edge that is 
 *        on the last position of the array
 * 
 * @param elist edge_list
 * @param index_to_pop index of the edge to pop
 */
void pop_edge(edge_list *elist, int index_to_pop){
    if (index_to_pop == (elist->n_links-1)) {
        elist->n_links--;
        return;
    }
    edge tmp = elist->data[index_to_pop];
    elist->n_links--;
    for (int i = index_to_pop ; i < elist->n_links; i++) {
        elist->data[i] = elist->data[i+1];
    }
    elist->data[elist->n_links] = tmp;
}

/**
 * @brief Implementation of the kruskal algorithm 
 * 
 * @param g graph 
 */
void kruskal_algo(graphl *g) {
    int i, cno1, cno2;
    int  *belongs = (int *) calloc(sizeof(int), g->n_vertex);
        
    qsort(g->elist->data, g->elist->n_links, sizeof(edge), cmp_cost_greater);
    for (i = 0; i < g->n_vertex; i++){
        make_set_no_size(i, belongs);
    }
    
    for (i = g->elist->n_links-1; i >= 0 ; i--) {
        cno1 = find_compressed(g->elist->data[i].u, belongs);
        cno2 = find_compressed(g->elist->data[i].v, belongs);
        
        if (cno1 != cno2) {
            pop_edge(g->elist, i);
            g->elist->mst = g->elist->n_links+1;
            link_set(cno2, cno1, belongs);
        }
    }
    free(belongs);
}

/**
 * @brief Depth first search algorithm modified to be used in David Bader's Algorithm
 * 
 * @param v Starting vertex
 * @param in Array that keeps the value of the counter when the vertex is first accessed
 * @param out Array that keeps the value of the counter when the vertex is exited
 * @param counter number of edges traversed
 * @param parents vertex used to access the next vertex
 * @param mst mst 
 */
void dfs_mst(int v, int * in, int *out, int *counter, int *parents, edge_list * mst){
    (*counter)++;
    in[v] = *counter;
        
    for (int i = 0; i < mst->n_links; i++) {
        edge e = mst->data[i];
        if (e.v == v && !in[e.u]) {
            parents[e.u] = v;
            dfs_mst(e.u, in, out, counter, parents, mst);
            (*counter)++;
        }
        if (e.u == v && !in[e.v]) {
            parents[e.v] = v;
            dfs_mst(e.v, in, out, counter, parents, mst);
            (*counter)++;
        }
    }
    out[v] = *counter;
}

/**
 * @brief See https://arxiv.org/pdf/1908.03473.pdf
 * 
 * @param s s
 * @param t t
 * @param e e
 * @param in in Array that keeps the value of the counter when the vertex is first accessed
 * @param out Array that keeps the value of the counter when the vertex is exited 
 * @param parents vertex used to access the next vertex 
 * @param set disjoint set
 * @param MST mst
 * @param redges output containing replacement edges 
 */
void path_label(int s, int t, edge e, int *in, int *out, int *parents, int *set, edge_list *MST ,edge_list *redges){
    int plan = 0;
    int k1 = 0, k2 = 0;
    if (in[s] < in[t] && in[t] < out[s]) { //s is ancestor of t
        return;
    }
    if (in[t] < in[s] && in[s] < out[t]) {  //t is ancestor of s
        plan = 1; //ancestor
        k1 = in[t];
        k2 = in[s];
    } else if (in[s] < in[t]) {
        plan = 2; //LEFT
        k1 = out[s];
        k2 = in[t];
    } else {
        plan = 3; //RIGHT
        k1 = out[t];
        k2 = in[s];
    }
    int v = s;
    while (k1 < k2) {
        if(find_compressed(v, set) == v){
            MST->data[redges->n_links].u = min(v, parents[v]); /* saves mst edge */
            MST->data[redges->n_links].v = max(v, parents[v]);
            redges->data[redges->n_links].u = min(e.u, e.v); /* saves corresponding best replacement edge */
            redges->data[redges->n_links].v = max(e.u, e.v);
            redges->n_links++;
            link_set(parents[v],v, set);
        }
        v = parents[v];
        switch (plan) {
            case 1:
                k2 = in[v];
                break;
            case 2:
                k1 = out[v];
                break;
            case 3:
                k2 = in[v];
                break;
            default:
                break;
        }
    }
}

//Encontra todas as ligações substitutas de uma MST
//Recebe uma MST e as ligações que não pertencem à MST ordenadas por custo crescente
//Retorna dois arrays de ligações: vetor contendo as ligações da MST, e outro que contém a sua substituta
/**
 * @brief Finds all the replacement edges for a mst
 * 
 * @param g graph 
 * @param nonMST edges that are not in the mst 
 * @param MST mst
 * @param v key of the starting vertex
 * @param r_edges returns replacement edges
 */
void find_replacement_edges(graphl *g, edge_list *nonMST, edge_list *MST, int v, edge_list * r_edges){ // v is KEY(vertex)
    int counter = 0;
    int *in = (int *) calloc(sizeof(int), g->n_vertex);
    int *out = (int *) calloc(sizeof(int), g->n_vertex);
    int *set = (int *) calloc(sizeof(int), g->n_vertex); // saves size of disjoint set
    int *parents = (int *) calloc(sizeof(int), g->n_vertex);
    
    parents[v] = v;
    counter = 0;
    dfs_mst(v, in, out, &counter, parents, MST);
    
    for (int i = 0; i < g->n_vertex; i++) {
        make_set_no_size(i, set);
    }
    
    for (int i = r_edges->n_links ; i < MST->n_links ; i++) {
        r_edges->data[i].u = -1;
        r_edges->data[i].v = -1;
    }
    
    
    for (int i = nonMST->n_links -1; i >= 0; i--) {
        edge e = nonMST->data[i];
        path_label(e.u, e.v, e, in, out, parents, set, MST, r_edges);
        path_label(e.v, e.u, e, in, out, parents, set, MST, r_edges);
    }
    free(in);
    free(out);
    free(set);
    free(parents);
}

/**
 * @brief Finds all the strongly connected components in an edgelist returns the strongly connected components
 * 
 * @param elist edge list
 * @param r_vertex vertex to remove
 * @param g graph
 * @param sz array of sizes
 * @return int* 
 */
int * findscc(edge_list elist, int r_vertex, graphl *g, int *sz){
    int i;
    int *set = (int *) calloc(sizeof(int), g->n_vertex);

    for ( i = 0 ; i < g->n_vertex ; i++){
        make_set(i,set,sz);
    }

    for ( i = 0; i < elist.n_links; i++){
        int u = elist.data[i].u;
        int v = elist.data[i].v;
        if(u != r_vertex && v != r_vertex){
            set_union( u , v , set, sz);
        }
    }
    return set;
}

/**
 * @brief Maps all the strongly connected components ignoring r_vertex, returns the new edge_list that doesn't 
 *        contain the vertex aswell as the number of replaced edges
 * 
 * @param r_vertex vertex to replace 
 * @param elist edge_list
 * @param mst mst
 * @param g graph
 * @param result variable with number of replaced edges
 * @param newlist mst that doesn't contain the vertex
 */
void connectscc(int r_vertex, edge_list * elist, edge_list mst, graphl *g, int * result, edge_list *newlist){
    int i;
    int *original_sz = (int *) calloc(sizeof(int), g->n_vertex);
    int *new_sz = (int *) calloc(sizeof(int), g->n_vertex);
    int *original_set = findscc(mst, -1, g, original_sz);
    int *new_set = findscc(mst, r_vertex, g, new_sz);
    int vertex_set = find_compressed( r_vertex, original_set);

    for(i = elist->n_links-1; i>= 0; i--){
        int ucomp = find_compressed(elist->data[i].u, original_set);
        int vcomp = find_compressed(elist->data[i].v, original_set);
        int new_ucomp = find_compressed(elist->data[i].u, new_set);
        int new_vcomp = find_compressed(elist->data[i].v, new_set);
        
        if(ucomp == vertex_set && vcomp == vertex_set && new_ucomp != new_vcomp ){
            if(elist->data[i].u != r_vertex && elist->data[i].v != r_vertex){
                set_union( elist->data[i].u , elist->data[i].v , new_set, new_sz);
                (*newlist).data[(*newlist).n_links] = elist->data[i];
                (*newlist).n_links++;
                (*result)++;
                if(find_size(elist->data[i].u,new_sz, new_set) == find_size(r_vertex,original_sz, original_set)-1){
                    break;
                }
            }
        }
    }
    free(original_sz);
    free(new_sz);
    free(new_set);
    free(original_set);
}
