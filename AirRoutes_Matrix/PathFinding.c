//
//  PathFinding.c
//  
//
//  Created by André Venceslau and Alexandre Lopes on 05/12/2020.
//

#include "PathFinding.h"
#include "Matrix.h"
#include "AirRoutes0.h"
#include "DisjointSet.h"

#include <stdlib.h>
#define EXCH(A,B) { edge t = A ; A=B ; B=t;}

bool GREATER(double A, double B){
    return (((A) > (B)) ? (true) : (false));
}

bool LESS(double A, double B){
    return (((A) < (B)) ? (true) : (false));
}

void CleanEdgeList(edge_list *e){
    free(e->data);
}

int SOffset(edge a){
    return Offset(a.u, a.v);
}

void PopEdge(edge_list *elist, int index_to_pop){
    if (index_to_pop == (elist->n_links-1)) {
        elist->n_links--;
        return;
    }
    edge tmp = elist->data[index_to_pop];
    elist->n_links--;
    for (int i = index_to_pop ; i < elist->n_links; i++) {
        elist->data[i] = elist->data[i+1];
    }
    elist->data[elist->n_links] = tmp;
}

int Partition(edge_list *a, graphm *g, int l, int r, bool (*compare) (double, double) ) {
    int i, j;
    edge e = a->data[r];
    i = l-1;
    j = r;
    for (;;) {
        while (compare(g->matrix[SOffset(a->data[++i])], g->matrix[SOffset(e)]));
        while (compare(g->matrix[SOffset(e)], g->matrix[SOffset(a->data[--j])]))
            if (j == l) break;
        if (i >= j) break;
        EXCH(a->data[i], a->data[j]);
    }
    EXCH(a->data[i], a->data[r]);
    return i;
}


void QuickSortRecursive(edge_list *a, graphm *g, int l, int r, bool (*compare) (double, double) ) {
    if (r <= l) return;
    int i = Partition(a, g, l, r, compare); 
    QuickSortRecursive(a, g,l, i-1, compare); 
    QuickSortRecursive(a, g, i+1, r, compare);
}

// Applying Krushkal Algo
void kruskalAlgo(graphm *g, edge_list * elist) {
    int i, j, cno1, cno2;
    
    int  *belongs = (int *) calloc(sizeof(int), g->n_vertex);
    if (belongs == NULL) { CleanEdgeList(elist); AbortMatrix(&g); }
    
    elist->n_links = 0;
    
    for (i = 1; i < g->n_vertex; i++){
        for (j = 0; j < i; j++) {
            if (g->matrix[Offset(i, j)] != 0) {
                elist->data[elist->n_links].u = min(i,j);
                elist->data[elist->n_links].v = max(i,j);
                elist->n_links++;
            }
        }
    }
    
    QuickSortRecursive(elist, g, 0, elist->n_links-1, GREATER);
    
    for (i = 0; i < g->n_vertex; i++){
        makesetnosize(i, belongs);
    }
    
    for (i = elist->n_links-1; i >= 0 ; i--) {
        cno1 = findwcompression(elist->data[i].u, belongs);
        cno2 = findwcompression(elist->data[i].v, belongs);
        
        if (cno1 != cno2) {
            PopEdge(elist, i);
            elist->mst = elist->n_links+1;
            linkset(cno2, cno1, belongs);
        }
    }
    free(belongs);
}

void DFSMST(int v, int * in, int *out, int *counter, int *parents, edge_list * mst){
    (*counter)++;
    in[v] = *counter;
        
    for (int i = 0; i < mst->n_links; i++) {
        edge e = mst->data[i];
        if (e.v == v && !in[e.u]) {
            parents[e.u] = v;
            DFSMST(e.u, in, out, counter, parents, mst);
            (*counter)++;
        }
        if (e.u == v && !in[e.v]) {
            parents[e.v] = v;
            DFSMST(e.v, in, out, counter, parents, mst);
            (*counter)++;
        }
    }
    out[v] = *counter;
}

void PathLabel(int s, int t, edge e, int *in, int *out, int *parents, int *set, edge_list *MST ,edge_list *redges){
    int plan = 0;
    int k1 = 0, k2 = 0;
    if (in[s] < in[t] && in[t] < out[s]) { //s is ancestor of t
        return;
    }
    if (in[t] < in[s] && in[s] < out[t]) {  //t is ancestor of s
        plan = 1; //ancestor
        k1 = in[t];
        k2 = in[s];
    } else if (in[s] < in[t]) {
        plan = 2; //LEFT
        k1 = out[s];
        k2 = in[t];
    } else {
        plan = 3; //RIGHT
        k1 = out[t];
        k2 = in[s];
    }
    int v = s;
    while (k1 < k2) {
        if(findwcompression(v, set) == v){
            
            //data type edge u = menor vertex, v = maior vertex
            MST->data[redges->n_links].u = min(v, parents[v]); //saves mst edge
            MST->data[redges->n_links].v = max(v, parents[v]);
            redges->data[redges->n_links].u = min(e.u, e.v); //saves corresponding best replacement edge
            redges->data[redges->n_links].v = max(e.u, e.v);
            redges->n_links++;
            linkset(parents[v],v, set);
        }
        v = parents[v];
        switch (plan) {
            case 1:
                k2 = in[v];
                break;
            case 2:
                k1 = out[v];
                break;
            case 3:
                k2 = in[v];
                break;
            default:
                break;
        }
    }
}

//mst and non mst must be ordered
void FindReplacementEdges(graphm *g, edge_list *nonMST, edge_list *MST, int v, edge_list * r_edges){ // v is KEY(vertex)
    int counter = 0;
    int *in = (int *) calloc(sizeof(int), g->n_vertex);
    if (in == NULL) { CleanEdgeList(nonMST); CleanEdgeList(MST); CleanEdgeList(r_edges); AbortMatrix(&g); }
    int *out = (int *) calloc(sizeof(int), g->n_vertex);
    if (out == NULL) { free(in); CleanEdgeList(nonMST); CleanEdgeList(MST); CleanEdgeList(r_edges); AbortMatrix(&g); }
    int *set = (int *) calloc(sizeof(int),g->n_vertex); // saves size of disjoint set
    if (set == NULL) { free(out); free(in); CleanEdgeList(nonMST); CleanEdgeList(MST); CleanEdgeList(r_edges); AbortMatrix(&g); }

    int *parents = (int *) malloc(sizeof(int)*g->n_vertex);
    if (parents == NULL) { free(set); free(out); free(in); CleanEdgeList(nonMST); CleanEdgeList(MST); CleanEdgeList(r_edges); AbortMatrix(&g); }

    for (int i = 0; i < g->n_vertex; i++) {
        parents[i] = 0;
    }
    
    parents[v] = v;
    counter = 0;
    DFSMST(v, in, out, &counter, parents, MST);
    
    for (int i = 0; i < g->n_vertex; i++) {
        makesetnosize(i, set);
    }
    
    for (int i = r_edges->n_links ; i < MST->n_links ; i++) {
        r_edges->data[i].u = -1;
        r_edges->data[i].v = -1;
    }
    
    
    for (int i = nonMST->n_links -1; i >= 0; i--) {
        edge e = nonMST->data[i];
        PathLabel(e.u, e.v, e, in, out, parents, set, MST, r_edges);
        PathLabel(e.v, e.u, e, in, out, parents, set, MST, r_edges);
    }
    free(in);
    free(out);
    free(set);
    free(parents);
}

int * findscc(edge_list elist, int r_vertex, graphm *g, int *sz){
    int i;
    int *set = (int *) calloc(sizeof(int), g->n_vertex);

    for ( i = 0 ; i < g->n_vertex ; i++){
        makeset(i,set,sz);
    }

    for ( i = 0; i < elist.n_links; i++){
        int u = elist.data[i].u;
        int v = elist.data[i].v;
        if(u != r_vertex && v != r_vertex){
            SetUnion( u , v , set, sz);
        }
    }
    return set;
}

void connectscc(int r_vertex, edge_list elist, edge_list mst, graphm *g, int * result, edge_list *newlist){
    int i;
    int *original_sz = (int *) calloc(sizeof(int), g->n_vertex);
    int *new_sz = (int *) calloc(sizeof(int), g->n_vertex);
    int *original_set = findscc(mst, -1, g, original_sz);
    int *new_set = findscc(mst, r_vertex, g, new_sz);
    int vertex_set = findwcompression( r_vertex, original_set);

    for(i = elist.n_links-1; i>= 0; i--){
        int ucomp = findwcompression(elist.data[i].u, original_set);
        int vcomp = findwcompression(elist.data[i].v, original_set);
        int new_ucomp = findwcompression(elist.data[i].u, new_set);
        int new_vcomp = findwcompression(elist.data[i].v, new_set);
        
        if(ucomp == vertex_set && vcomp == vertex_set && new_ucomp != new_vcomp ){
            if(elist.data[i].u != r_vertex && elist.data[i].v != r_vertex){
                SetUnion( elist.data[i].u , elist.data[i].v , new_set, new_sz);
                (*newlist).data[(*newlist).n_links] = elist.data[i];
                (*newlist).n_links++;
                (*result)++;
                if( findsize(elist.data[i].u,new_sz, new_set) == findsize(r_vertex,original_sz, original_set)-1){
                    break;
                }
            }
        }
    }
    free(original_sz);
    free(new_sz);
    free(new_set);
    free(original_set);
}
