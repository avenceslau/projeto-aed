//
//  DisjointSet.c
//  Projeto-Aed
//
//  Created by André Venceslau and Alexandre Lopes on 09/12/2020.
//

#include "DisjointSet.h"

int findwcompression(int x, int*set){
    int root = x;
    while (root != set[root]) {
        root = set[root];
    }
    while (x != root) {
        int next = set[x];
        set[x] = root;
        x = next;
    }
    return root;
}

void SetUnion(int x, int y, int * set, int * sz){
    x = findwcompression(x, set);
    y = findwcompression(y, set);
    
    if (x == y) {
        return;
    }
    if (sz[x] < sz[y]) {
        set[x] = y;
        sz[y] = sz[x] + sz[y];
    } else {
        set[y] = x;
        sz[x] = sz[x] + sz[y];
    }
}

void linkset(int x, int y, int * set){
    x = findwcompression(x, set);
    y = findwcompression(y, set);
    if (x == y) {
        return;
    }
    set[y] = x;
}

void makeset(int x, int*set, int *sz){
    if (set[x] == 0) {
        set[x] = x;
        sz[x] = 1;
    }
}

void makesetnosize(int x, int*set){
    if (set[x] == 0) {
        set[x] = x;
    }
}

int findsize(int x, int *sz, int * set){
    return sz[findwcompression(x, set)];
}
