//
//  AirRoutes0.h
//  Projeto-Aed
//
//  Created by André Venceslau and Alexandre Lopes on 03/12/2020.
//

#ifndef AirRoutes0_h
#define AirRoutes0_h

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

typedef struct Input _input;
struct Input{
    int n_total_airports;
    int n_total_links;
    int var0;
    int var1;
    char mode[3];
};

void MSelectMode(FILE *fp, FILE *outfp);
void Print(_input input, FILE * fpOut);
bool CheckVertex(int n_airports, int vertex_to_check);

#endif /* AirRoutes0_h */
