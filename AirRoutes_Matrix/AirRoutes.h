//
//  AirRoutes.h
//  Projeto-Aed
//
//  Created by André Venceslau and Alexandre Lopes on 03/12/2020.
//

#ifndef AirRoutes_h
#define AirRoutes_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "PathFinding.h"

void SelectMode(FILE *fp, FILE *outfp);
bool Compare(edge a, edge b);


#endif /* AirRoutes_h */
