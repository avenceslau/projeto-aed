//
//  NoMatrix.c
//  Projeto-Aed
//
//  Created by André Venceslau on 10/12/2020.
//

#include "NoMatrix.h"
#include "PathFinding.h"

void EraseGraph(graphl *g){
    g->n_links = 0;
    g->n_vertex = 0;
    free(g->cost);
    free(g->elist);
}



void InitGraph (graphl *g){
    g->n_links = 0;
    g->n_vertex = 0;
    g->elist->mst = 0;
    g->elist->n_links = 0;
}

graphl * CreateGraph(int n_links){
    graphl *g = (graphl *) malloc(sizeof(graphl));
    g->elist->data = (edge *) malloc(sizeof(edge)*n_links);
    InitGraph(g);
    return g;
}

void ElistStoreData(int from, int to, double cost, graphl *g){
    g->elist->data[g->elist->n_links].u = min(KEY(from), KEY(to));
    g->elist->data[g->elist->n_links].v = max(KEY(from), KEY(to));
    g->cost[g->elist->n_links] = cost;
    g->elist->n_links++;
}

void ElistGetData(graphl *g, FILE *fp){
    int from = 0, to = 0;
    double cost = 0;
    
    for (int i = 0 ; i < g->n_links ; i++) {
        fscanf(fp, "%d %d %lf", &from, &to, &cost);
        ElistStoreData(from, to, cost, g);
    }
}
