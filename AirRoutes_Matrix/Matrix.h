//
//  Matrix.h
//  Projeto-Aed
//
//  Created by André Venceslau and Alexandre Lopes on 19/11/2020.
//

#ifndef Matrix_h
#define Matrix_h

#include <stdio.h>
#include <stdbool.h>

#define KEY(x) (x-1)
#define VERTEX(x) (x+1)
#define max(i,j) (((i) >= (j)) ? (i) : (j))
#define min(i,j) (((i) <= (j)) ? (i) : (j))

typedef struct {
    double *matrix;
    int n_vertex;
    int n_links;
} graphm;

int Offset(int i, int j);
void GetDataMatrix(int n_links , FILE *fp, graphm ** g, int max_vertex);
double * CreateMatrix(int n_vertex);
void MatrixInit(graphm **G);
void EraseMatrix(graphm **g);
void AbortMatrix(graphm **g);

#endif /* Matrix_h */
