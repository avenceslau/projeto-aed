//
//  PathFinding.h
//  
//
//  Created by André Venceslau and Alexandre Lopes on 05/12/2020.
//

#ifndef PathFinding_h
#define PathFinding_h

#include <stdio.h>
#include "Matrix.h"

typedef struct edge {
    int u, v;
} edge;

typedef struct edge_list {
    edge *data;
    int n_links;
    int mst;
} edge_list;

void kruskalAlgo(graphm *g, edge_list * elist);
void QuickSortEDGE(edge_list *a,  int l, int r);
void QuickSortRecursive(edge_list *a, graphm *g, int l, int r, bool (*compare) (double, double) );
bool LESS(double A, double B);
bool GREATER(double A, double B);
int SOffset(edge a);
void FindReplacementEdges(graphm *g, edge_list *nonMST, edge_list *MST, int v, edge_list * r_edges);
void CleanEdgeList(edge_list *e);
int * findscc(edge_list elist, int r_vertex, graphm *g, int *sz);
void connectscc(int r_vertex, edge_list elist, edge_list mst, graphm *g, int * result, edge_list *newlist);

#endif /* PathFinding_h */
