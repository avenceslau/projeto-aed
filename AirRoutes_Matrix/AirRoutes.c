//
//  AirRoutes.c
//  Projeto-Aed
//
//  Created by André Venceslau and Alexandre Lopes on 03/12/2020.
//

#include "AirRoutes.h"
#include "AirRoutes0.h"
#include "Matrix.h"
#include "PathFinding.h"
#include "DisjointSet.h"

#define EXCH(A,B) { edge t = A ; A=B ; B=t;}


bool Compare(edge a, edge b){
    if(a.u < b.u){
        return true;
    } else if (a.u == b.u) {
        if (a.v < b.v) {
            return true;
        }
    }
    return false;
}

int PartitionEDGE(edge_list *a,  int l, int r) {
    int i, j;
    edge e = a->data[r];
    i = l-1;
    j = r;
    for (;;) {
        while (Compare(a->data[++i], e));
        while (Compare(e, a->data[--j])){
            if (j == l) break;
        }
        if (i >= j) break;
        EXCH(a->data[i], a->data[j]);
    }
    EXCH(a->data[i], a->data[r]);
    return i;
}

void PrintE1(edge_list elist, edge_list mst, edge_list r_edges, graphm *g, FILE * fpOut){
    int counter = 0;
    for (int i = elist.mst-1 ; i < g->n_links ; i++) {
        if (counter < r_edges.n_links && elist.data[i].u == mst.data[counter].u && elist.data[i].v == mst.data[counter].v) {
            fprintf(fpOut, "%d %d %.2lf %d %d %.2lf\n",
                    VERTEX(elist.data[i].u), VERTEX(elist.data[i].v), g->matrix[SOffset(elist.data[i])],
                    VERTEX(r_edges.data[counter].u), VERTEX(r_edges.data[counter].v), g->matrix[SOffset(r_edges.data[counter])]);
            counter++;
        } else {
            fprintf(fpOut, "%d %d %.2lf -1\n", VERTEX(elist.data[i].u), VERTEX(elist.data[i].v), g->matrix[SOffset(elist.data[i])]);
        }
    }
}

int PartitionTWOEDGES(edge_list *a, edge_list *b,  int l, int r) {
    int i, j;
    edge e = a->data[r];
    i = l-1;
    j = r;
    for (;;) {
        while (Compare(a->data[++i], e));
        while (Compare(e, a->data[--j])){
            if (j == l) break;
        }
        if (i >= j) break;
        EXCH(a->data[i], a->data[j]);
        EXCH(b->data[i], b->data[j]);
    }
    EXCH(a->data[i], a->data[r]);
    EXCH(b->data[i], b->data[r]);
    return i;
}

void QuickSortEDGE(edge_list *a,  int l, int r) {
    if (r <= l) return;
    int i = PartitionEDGE(a, l, r);
    QuickSortEDGE(a, l, i-1);
    QuickSortEDGE(a, i+1, r);
}

void QuickSortTWOEDGES(edge_list *a, edge_list *b,  int l, int r) {
    if (r <= l) return;
    int i = PartitionTWOEDGES(a, b,l, r);
    QuickSortTWOEDGES(a, b,l, i-1);
    QuickSortTWOEDGES(a, b,i+1, r);
}

void PrintEdgeList(edge_list e, int start, int end, graphm *g, FILE *fpOut){
    for (int i = start; i < end; i++) {
        fprintf(fpOut, "%d %d %.2lf\n", VERTEX(e.data[i].u), VERTEX(e.data[i].v), g->matrix[SOffset(e.data[i])]);
    }
}

bool CheckInput(_input input){
    if (CheckVertex(input.n_total_airports, input.var0) && CheckVertex(input.n_total_airports, input.var1) && (input.var0 != input.var1)) {
        return true;
    } else {
        return false;
    }
}

edge FindReplacement(edge e, edge_list r_edges, edge_list MST){
    for (int i = 0; i < r_edges.n_links ; i++) {
        if (MST.data[i].u == e.u && MST.data[i].v == e.v) {
            return r_edges.data[i];
        }
    }
    edge z;
    z.u = -1;
    z.v = -1;
    return z;
}

edge_list GetMST(graphm *g, edge_list *nonMST, double * total_cost){
    edge_list mst;
    mst.data = (edge *) malloc(sizeof(edge)*(g->n_links - nonMST->n_links));
    if (mst.data == NULL) { CleanEdgeList(nonMST); AbortMatrix(&g); }
    mst.n_links = g->n_links - nonMST->n_links;
    (*total_cost) = 0;
    for (int i = 0; i < mst.n_links; i++) {
        mst.data[i] = nonMST->data[i+nonMST->mst-1];
        (*total_cost) += g->matrix[SOffset(mst.data[i])];
    }
    return mst;
}

int *FindComponents(edge_list *edges, graphm * g, int * counter){
    int * set = (int *) calloc(sizeof(int), g->n_vertex);
    if (set == NULL) { CleanEdgeList(edges); AbortMatrix(&g);}
    int * sz = (int *) calloc(sizeof(int), g->n_vertex);
    if (sz == NULL) { free(set); CleanEdgeList(edges); AbortMatrix(&g);}
    int * components = (int *) calloc(sizeof(int), g->n_vertex);
    if (components == NULL) { free(sz); free(set); CleanEdgeList(edges); AbortMatrix(&g);}
    
    for (int i = 0; i < g->n_vertex; i++) {
        makeset(i, set, sz);
        components[i] = -1;
    }
    
    int start_mst = g->n_links - edges->n_links;
    
    for (int i = 0; i < start_mst ; i++) {
        SetUnion(edges->data[i+edges->mst-1].u, edges->data[i+edges->mst-1].v, set, sz);
    }
    
    (*counter) = 0;
    int aux;
    free(sz);
    for (int i = 0; i < g->n_vertex; i++) {
        aux = findwcompression(i, set);
        if (aux != components[aux]) {
            components[aux] = aux;
            (*counter)++;
        }
    }
    free(set);
    int *rep = (int *) calloc(sizeof(int), (*counter));
    int end = (*counter);
    (*counter) = 0;
    for (int i = 0; (*counter) < end ; i++) {
        if (components[i] != -1) {
            rep[(*counter)] = components[i];
            (*counter)++;
        }
    }
    free(components);
    return rep;
}

void ModeA1(graphm * g, _input input, FILE * fpOut){
    edge_list elist;
    double total_cost;
    elist.data = (edge *) malloc(sizeof(edge)*g->n_links);
    if (elist.data == NULL) { AbortMatrix(&g); }
    
    kruskalAlgo(g, &elist);
    edge_list MST = GetMST(g, &elist, &total_cost);
    free(elist.data);
    QuickSortEDGE(&MST, 0, MST.n_links-1);
    Print(input, fpOut);
    fprintf(fpOut, " %d %.2lf\n", MST.n_links, total_cost);
    PrintEdgeList(MST, 0,MST.n_links, g, fpOut);
    free(MST.data);
}

void ModeB1(graphm *g, _input input, FILE *fpOut){
    edge_list elist, MST, r_edges;
    elist.data = (edge *) malloc(sizeof(edge)*g->n_links);
    if (elist.data == NULL) { AbortMatrix(&g); }
    edge e, replacement;
    replacement.u = -1; replacement.v = -1;
    bool found = false;
    double total_cost;
    
    Print(input, fpOut);
    kruskalAlgo(g, &elist);
    MST = GetMST(g, &elist, &total_cost);
    QuickSortEDGE(&MST, 0, MST.n_links-1);
    fprintf(fpOut, " %d %d %d %.2lf",input.var0,input.var1, MST.n_links, total_cost);
    r_edges.n_links = 0;
    if (CheckInput(input)) {
        int u = min(input.var0, input.var1), v = max(input.var0, input.var1);
        for (int i = 0; i < MST.n_links ; i++) {
            if (u == VERTEX(MST.data[i].u) && v == VERTEX(MST.data[i].v) && !found) {
                found = true;
                e = MST.data[i];
                break;
            }
        }
        if (found){
            r_edges.data = (edge *) malloc(sizeof(edge)*(MST.n_links));
            if (r_edges.data == NULL) { CleanEdgeList(&MST); CleanEdgeList(&elist); AbortMatrix(&g); }
            FindReplacementEdges(g, &elist, &MST, e.v, &r_edges);
            replacement = FindReplacement(e, r_edges, MST);
            free(r_edges.data);
        }
        if ((replacement.u == -1 && !g->matrix[Offset(KEY(input.var0), KEY(input.var1))]) || (replacement.u == -1 && found)) {
            fprintf(fpOut, " -1\n");
        } else if (g->matrix[Offset(KEY(input.var0), KEY(input.var1))] && replacement.u == -1){
            fprintf(fpOut, " 0\n");
        } else {
            fprintf(fpOut, " 1\n");
        }
    } else {
        fprintf(fpOut, " -1\n");
    }
    
    QuickSortEDGE(&elist, elist.mst-1, g->n_links-1);
    PrintEdgeList(elist, elist.mst-1, g->n_links, g, fpOut);
    if (replacement.u != -1) {
        fprintf(fpOut, "%d %d %.2lf\n", VERTEX(replacement.u), VERTEX(replacement.v), g->matrix[SOffset(replacement)]);
    }
    free(elist.data);
    free(MST.data);
}

void ModeC1(graphm * g, _input input, FILE * fpOut){
    edge_list elist, MST, r_edges;
    elist.data = (edge *) malloc(sizeof(edge)*g->n_links);
    if (elist.data == NULL) { AbortMatrix(&g); }
    edge e, replacement;
    double total_cost;
    bool found = false;

    
    replacement.u = -1; replacement.v = -1;
    r_edges.n_links = 0;
    Print(input, fpOut);
    kruskalAlgo(g, &elist);
    MST = GetMST(g, &elist, &total_cost);
    QuickSortEDGE(&MST, 0, MST.n_links-1);
    fprintf(fpOut, " %d %d %d %.2lf", input.var0, input.var1, MST.n_links, total_cost);
    if (CheckInput(input)) {
        int u = min(input.var0, input.var1), v = max(input.var0, input.var1);
        for (int i = 0; i < MST.n_links ; i++) {
            if (u == VERTEX(MST.data[i].u) && v == VERTEX(MST.data[i].v) && !found) {
                found = true;
                e = MST.data[i];
                break;
            }
        }
        if (found){
            r_edges.data = (edge *) malloc(sizeof(edge)*(MST.n_links));
            if (r_edges.data == NULL) { CleanEdgeList(&MST); CleanEdgeList(&elist); AbortMatrix(&g); }
            FindReplacementEdges(g, &elist, &MST, e.v, &r_edges);
            replacement = FindReplacement(e, r_edges, MST);
            free(r_edges.data);
        }
    }
    if (!g->matrix[Offset(KEY(input.var0), KEY(input.var0))] || !found) {
        fprintf(fpOut, " -1\n");
    }
    else {
        if (replacement.u == -1) {
            fprintf(fpOut, " %d %.2lf\n", MST.n_links-1, (total_cost-(g->matrix[Offset(KEY(input.var0), KEY(input.var1))])));
        } else {
            fprintf(fpOut, " %d %.2lf\n", MST.n_links, (total_cost-(g->matrix[Offset(KEY(input.var0), KEY(input.var1))])+(g->matrix[SOffset(replacement)])));
        }
        
    }
    QuickSortEDGE(&elist, elist.mst-1, g->n_links-1);
    PrintEdgeList(elist, elist.mst-1, g->n_links, g, fpOut);
    if (replacement.u != -1 || found) {
        for (int i = elist.mst-1 ; i < g->n_links ; i++) {
            if (e.u == elist.data[i].u && e.v == elist.data[i].v) {
                elist.data[i] = replacement;
            }
        }
        QuickSortEDGE(&elist, elist.mst-1, g->n_links-1);
        if (replacement.u == -1) {
            PrintEdgeList(elist, elist.mst, g->n_links, g, fpOut);
        } else {
            PrintEdgeList(elist, elist.mst-1, g->n_links, g, fpOut);
        }
        
        
        
    }
    free(elist.data);
    free(MST.data);}

void ModeD1(graphm * g, _input input, FILE * fpOut){
    edge_list elist;
    double total_cost;
    int result = 0;
    elist.data = (edge *) malloc(sizeof(edge)*g->n_links);
    if (elist.data == NULL) { AbortMatrix(&g); }

    kruskalAlgo(g, &elist);
    edge_list mst = GetMST(g, &elist, &total_cost);
    QuickSortEDGE(&mst, 0, mst.n_links-1);
    int r_vertex = input.var0;
    if(!CheckVertex(g->n_vertex, r_vertex)){
        result = -1;
    }

    edge_list newlist;
    newlist.data = (edge *) malloc(sizeof(edge)*g->n_links);
    newlist.n_links = 0;
    connectscc(KEY(r_vertex),elist,mst,g, &result, &newlist);
    Print(input,fpOut);
    QuickSortEDGE(&newlist, 0, newlist.n_links-1);
    fprintf(fpOut," %d %d %.2lf %d\n", r_vertex, mst.n_links, total_cost, result);
    for ( int i = 0; i < mst.n_links; i++){
        
        fprintf(fpOut,"%d %d %.2lf\n", VERTEX(mst.data[i].u), VERTEX(mst.data[i].v), g->matrix[Offset(mst.data[i].u,mst.data[i].v)]);
    }
    for ( int i = 0; i < newlist.n_links; i++){
        fprintf(fpOut,"%d %d %.2lf\n", VERTEX(newlist.data[i].u), VERTEX(newlist.data[i].v), g->matrix[Offset(newlist.data[i].u,newlist.data[i].v)]);
    }
    free(elist.data);
    free(mst.data);
    free(newlist.data);
}

void ModeE1(graphm * g, _input input, FILE * fpOut){
    edge_list elist, mst, r_edges;
    int counter = 0;
    double total_cost;

    elist.data = (edge *) malloc(sizeof(edge)*g->n_links);
    if (elist.data == NULL) { AbortMatrix(&g); }
    
    kruskalAlgo(g, &elist);
    QuickSortRecursive(&elist, g, elist.mst, g->n_links-1, LESS);
    int *rep = FindComponents(&elist, g, &counter);
    Print(input, fpOut);
    mst = GetMST(g, &elist, &total_cost);
    fprintf(fpOut, " %d %.2lf\n", mst.n_links, total_cost);
    r_edges.data = (edge *) malloc(sizeof(edge)*(mst.n_links));
    r_edges.n_links = 0;
    if (r_edges.data == NULL) { CleanEdgeList(&mst); CleanEdgeList(&elist); AbortMatrix(&g); }
    for (int i = 0; i < counter; i++) {
        FindReplacementEdges(g, &elist, &mst, rep[i], &r_edges); //instead of 0 takes redges.n_links
    }
    QuickSortTWOEDGES(&mst, &r_edges, 0, r_edges.n_links-1);
    QuickSortEDGE(&elist, elist.mst-1, g->n_links-1);
    PrintE1(elist, mst, r_edges, g, fpOut);
    free(rep);
    free(elist.data);
    free(mst.data);
    free(r_edges.data);
}

void SelectMode(FILE *fp, FILE *outfp){
    _input input;
    graphm *g = (graphm *) malloc(sizeof(graphm));
    
    if (g == NULL) {
        return;
    }
    int max_vertex = 0;
    while(fscanf(fp, "%d %d %s", &input.n_total_airports, &input.n_total_links, input.mode) != EOF){
        g->n_vertex = input.n_total_airports;
        g->n_links = input.n_total_links;
        g->matrix = CreateMatrix(g->n_vertex);
        MatrixInit(&g);
        if ((strcmp(input.mode, "B1")==0 ) || (strcmp(input.mode, "C1") == 0)) {
            fscanf(fp, "%d %d", &input.var0, &input.var1);
        }  else if (strcmp(input.mode, "D1")==0){
            fscanf(fp, "%d", &input.var0);
        }
        
        GetDataMatrix(input.n_total_links, fp, &g, max_vertex);
        if (strcmp(input.mode, "A1") == 0){ ModeA1(g, input, outfp); }
        else if (strcmp(input.mode, "B1") == 0){ ModeB1(g, input, outfp); }
        else if (strcmp(input.mode, "C1") == 0){ ModeC1(g, input, outfp); }
        else if (strcmp(input.mode, "D1") == 0){ ModeD1(g, input, outfp); }
        else if (strcmp(input.mode, "E1") == 0){ ModeE1(g, input, outfp); }
        fprintf(outfp,"\n");
        EraseMatrix(&g);
    }
    if (g != NULL) {
        free(g);
    }
}
