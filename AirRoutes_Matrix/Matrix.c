//
//  Matrix.c
//  Projeto-Aed
//
//  Created by André Venceslau and Alexandre Lopes on 19/11/2020.
//

#include "Matrix.h"
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>




int Offset(int i, int j){
    int a = min(i, j);
    int b = max(i, j);
    return a+ b*(b+1)/2;
}

void EraseMatrix(graphm **g){
    free((*g)->matrix);
}

void AbortMatrix(graphm **g){
    EraseMatrix(g);
    free(*g);
    exit(0);
}

void MatrixInit(graphm **G){
    for (int i = 0 ; i < ((*G)->n_vertex) ; i++) {
        for (int j = 0; j < (*G)->n_vertex; j++) {
            int o = Offset(i, j);
            (*G)->matrix[o] = 0;
        }
    }
}

double * CreateMatrix(int n_vertex){
    int m_size = n_vertex + n_vertex*(n_vertex-1)/2;

    double * matrix = (double *) malloc(sizeof(double)*m_size); //vertex^2*sizeof vertex
    if (matrix == NULL){
        free(matrix);
        exit(0);
    }
    return matrix;
}

void StoreData(int from, int to, double cost, graphm **g){
    (*g)->matrix[Offset(KEY(from), KEY(to))] = cost;
    (*g)->matrix[Offset(KEY(from), KEY(from))]++;
    (*g)->matrix[Offset(KEY(to), KEY(to))]++;
}

void GetDataMatrix(int n_links, FILE *fp, graphm ** g, int max_vertex){
    int i,from = 0, to = 0;
    double cost = 0;
    
    for (i = 1; i <= n_links ; i++) {
        fscanf(fp, "%d %d %lf", &from, &to, &cost);
        StoreData(from, to, cost, g);
    }
}
