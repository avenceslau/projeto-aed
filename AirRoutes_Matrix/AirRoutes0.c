//
//  AirRoutes0.c
//  Projeto-Aed
//
//  Created by André Venceslau and Alexandre Lopes on 03/12/2020.
//

#include "AirRoutes0.h"
#include "Matrix.h"

void Print(_input input, FILE * fpOut){
    fprintf(fpOut, "%d %d %s", input.n_total_airports, input.n_total_links, input.mode);
}


//Checks existence of a vertex
bool CheckVertex(int n_airports, int vertex_to_check){
    if (vertex_to_check > n_airports || vertex_to_check < 1) {
        return false;
    } else {
        return true;
    }
}

bool MFindTriangle(double * matrix, graphm * g, int vertex){
    int n_vertex = g->n_vertex;
    
    int l = matrix[Offset(KEY(vertex), KEY(vertex))];
    int * array = (int *) malloc(sizeof(int)*l);
    
    for (int i = 0, t = 0; i < n_vertex; i++) {
        if ((matrix[Offset(KEY(vertex), i)] != 0) && (i != KEY(vertex))) {
            array[t] = i+1;
            t++;
            
        }
    }
    
    for (int i = 0 ; i < l -1; i++) {
        for (int j = i+1; j < l; j++) {
            if (matrix[Offset(KEY(array[i]) , KEY(array[j]))] != 0){
                if (array != NULL) {
                    free(array);
                }
                return true;
            }
        }
    }
    if (array != NULL) {
        free(array);
    }
    return false;
}


//Counts number of triangles
int CountTriangle(double * matrix, graphm * g, _input input){
    int triangles = 0;
    int n_links = matrix[Offset(KEY(input.var0), KEY(input.var0))];
    int * array = (int *) malloc(sizeof(int)*n_links);
    int j;
    if (array == NULL) {
        free(array);
        AbortMatrix(&g);
    }
    
    for (int i = 0, t = 0; i < g->n_vertex; i++) {
        if ((matrix[Offset(KEY(input.var0), i) ] != 0) && (i != KEY(input.var0))) {
            array[t] = i+1;
            t++;
        }
    }
    
    for (int i = 0; i < n_links-1 ; i++) {
        for (j = i+1 ; j < n_links; j++) {
            if (matrix[Offset(KEY(array[i]), KEY(array[j]))] != 0) {
                triangles++;
            }
        }
    }
    if (array != NULL) {
        free(array);
    }
    return triangles;
}

void ModeA0(graphm *g, _input input, FILE *fpOut){
    double * matrix = g->matrix;
    Print(input, fpOut);
    if (CheckVertex(input.n_total_airports, input.var0)) {
        fprintf(fpOut, " %d %d\n", input.var0, (int) matrix[Offset(KEY(input.var0), KEY(input.var0))]);
    } else {
        fprintf(fpOut," %d %d\n", input.var0, -1);
    }
}

void ModeB0(graphm * g, _input input, FILE *fpOut){
    int from = input.var0, to = input.var1;
    double * matrix = g->matrix;
    Print(input, fpOut);
    if (CheckVertex(input.n_total_airports, from) && CheckVertex(input.n_total_airports, to) && (input.var0 != input.var1)) {
        if (matrix[Offset(KEY(from), KEY(to))] == 0) {
              fprintf(fpOut," %d %d %d\n", from, to, -1);
        } else {
              fprintf(fpOut, " %d %d %.2lf\n", from, to, matrix[Offset(KEY(from), KEY(to))]);
        }
    } else {
          fprintf(fpOut," %d %d %d\n", from, to, -1);
    }
}

void ModeC0(graphm *g, _input input, FILE *fpOut){
    Print(input, fpOut);
    if (CheckVertex(input.n_total_airports, input.var0)) {
        if (MFindTriangle(g->matrix, g, input.var0) == true) {
              fprintf(fpOut, " %d %d\n", input.var0, 1);
        } else {
              fprintf(fpOut, " %d %d\n", input.var0, 0);
        }
    } else {
          fprintf(fpOut, " %d %d\n", input.var0, -1);
    }
}

void ModeD0(graphm * g, _input input, FILE *fpOut){
    int k = 0;
    Print(input, fpOut);;
        if (CheckVertex(input.n_total_airports, input.var0)) {
            k = CountTriangle(g->matrix, g, input);
            if (k > 0) {
                fprintf(fpOut, " %d %d\n", input.var0, k);
            } else {
                fprintf(fpOut, " %d %d\n", input.var0, 0);
            }
        } else {
            fprintf(fpOut, " %d %d\n", input.var0, -1);
        }
}

void MSelectMode(FILE *fp, FILE *outfp){
    _input input;
    graphm *g = (graphm *) malloc(sizeof(graphm));
    
    if (g == NULL) {
        return;
    }
    int max_vertex = 0;
    
    while(fscanf(fp, "%d %d %s", &input.n_total_airports, &input.n_total_links, input.mode) != EOF){
        g->n_vertex = input.n_total_airports;
        g->matrix = CreateMatrix(g->n_vertex);
        g->n_links = input.n_total_links;
        MatrixInit(&g);
        if (strcmp(input.mode, "B0") == 0) {
            fscanf(fp, "%d %d", &input.var0, &input.var1);
        }  else {
            fscanf(fp, "%d", &input.var0);
        }
        
        GetDataMatrix(input.n_total_links , fp, &g, max_vertex);
        if (strcmp(input.mode, "A0") == 0){ ModeA0(g, input, outfp); }
        else if (strcmp(input.mode, "B0") == 0){ ModeB0(g, input, outfp); }
        else if (strcmp(input.mode, "C0") == 0){ ModeC0(g, input, outfp); }
        else if (strcmp(input.mode, "D0") == 0){ ModeD0(g, input, outfp); }
        fprintf(outfp,"\n");
        EraseMatrix(&g);
    }
    if (g != NULL) {
        free(g);
    }
}
