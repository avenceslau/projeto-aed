//
//  NoMatrix.h
//  Projeto-Aed
//
//  Created by André Venceslau on 10/12/2020.
//

#ifndef NoMatrix_h
#define NoMatrix_h

#include <stdio.h>
#include <stdlib.h>
#include "PathFinding.h"

typedef struct{
    int n_links;
    int n_vertex;
    edge_list *elist;
    double * cost;
} graphl;

#endif /* NoMatrix_h */
