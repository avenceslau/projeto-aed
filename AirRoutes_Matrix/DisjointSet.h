//
//  DisjointSet.h
//  Projeto-Aed
//
//  Created by André Venceslau and Alexandre Lopes on 09/12/2020.
//

#ifndef DisjointSet_h
#define DisjointSet_h

#include <stdio.h>
int findwcompression(int x, int*set);
void SetUnion(int x, int y, int * set, int * sz);
void linkset(int x, int y, int * set);
void makeset(int x, int*set, int *sz);
void makesetnosize(int x, int*set);
int findsize(int x, int *sz, int * set);

#endif /* DisjointSet_h */
