//
//  main.c
//  AirRoutes-Matrix
//
//  Created by André Venceslau and Alexandre Lopes on 19/11/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AirRoutes0.h"
#include "AirRoutes.h"


FILE * OpenFl(char * filename, char * mode){
    FILE * fp = NULL;
    fp = fopen(filename, mode);
    
    if (fp == NULL) {
        exit(0);
    }
    return fp;
}

bool CheckProblem(char * file_ext) {
    if (!strcmp(file_ext, ".routes0")) {
        return true;
    } else {
        return false;
    }
}

int main(int argc, const char * argv[]) {
    FILE *fp, *outfp;
    char * filename = (char *) malloc(strlen(argv[1])+1);
    strcpy(filename, argv[1]);
    fp = OpenFl(filename, "r");
    
    char *outname = strrchr(filename,'.');
    if(CheckProblem(outname)){
        *outname = '\0';
        strcat(filename,".queries");
        outfp = OpenFl(filename, "w");
        free(filename);
        MSelectMode(fp, outfp);
    } else {
        *outname = '\0';
        strcat(filename,".bbones");
        outfp = OpenFl(filename, "w");
        free(filename);
        SelectMode(fp, outfp);
    }
    fclose(fp);
    fclose(outfp);
    return 0;
}
